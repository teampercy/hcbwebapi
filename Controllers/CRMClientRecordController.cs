using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Net.Http.Headers;
//using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;


namespace HCBNewAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class CRMClientRecordController : ControllerBase
    {
        CRMClientRecord objCRMClientRecord;
        IConfiguration _configuration;
        private int GetUserId()
        {
            return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
        }

        public CRMClientRecordController(IConfiguration configuration, CRMClientRecord CRMClientRecordayer)
        {
            _configuration = configuration;
            objCRMClientRecord = CRMClientRecordayer;
        }

        [HttpPost]

        public IActionResult GetCRMClientList([FromBody]GetCRMClientRecordGrid model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetClientRecordList(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                [HttpGet]

        public IActionResult GetClientProcessInfoById(int ClientID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMClientRecord.GetClientProcessInfoById(ClientID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetClientRecordById(int ClientID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMClientRecord.GetClientRecordById(ClientID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
  [HttpPost]
        public IActionResult SaveCRMClientProcessInfo(SaveUpdateClientProcessInfoViewModel model)
        {
             int UserId = GetUserId();
             if(  model.SaveUpdateClientInfo.EncryptedPassword==""){}else{
            model.SaveUpdateClientInfo.EncryptedPassword = Encryption.Encrypt(model.SaveUpdateClientInfo.EncryptedPassword);
             }
           model.SaveUpdateClientInfo.LastModifiedBy = UserId;
      HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMClientRecord.AddClientProcessInfo(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateCRMClientProcessInfo([FromBody]SaveUpdateClientProcessInfoViewModel model)
        {
             int UserId = GetUserId();
             if(model.SaveUpdateClientInfo.EncryptedPassword==null||model.SaveUpdateClientInfo.EncryptedPassword==""){}
             else{
            model.SaveUpdateClientInfo.EncryptedPassword = Encryption.Encrypt(model.SaveUpdateClientInfo.EncryptedPassword);
             }
           model.SaveUpdateClientInfo.LastModifiedBy = UserId;
      HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMClientRecord.UpdateClientProcessInfo(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

           
        public IActionResult GetEmailAddress(int ClientId)
        {
             HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                 return Ok(new { result = objCRMClientRecord.GetEmailAddress(ClientId) });
            }
             catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

public IActionResult DeleteEmailList(int Id)
{
        {
             HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                 return Ok(new { result = objCRMClientRecord.DeleteEmailList(Id) });
            }
             catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
}
        [HttpPost]
        public IActionResult SaveCRMClientRecord([FromBody]SaveContactViewModel model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.saveCRMClientRecord.CreatedBy = userid;
                return Ok(new { result = objCRMClientRecord.SaveCrmClientRecord(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    [HttpGet]
        public IActionResult SendNotificationEmail(string CompanyName, String Fullname, int Noteid)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
             int userid = GetUserId();   
                return Ok(new { result = objCRMClientRecord.SendNoteEmail(CompanyName, userid, Fullname, Noteid) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
      [HttpGet]
        public IActionResult SendNewClientAddedEmail(string Fullname, string CompanyName )
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();   
                return Ok(new { result = objCRMClientRecord.SendNewClientAddedEmail(Fullname, userid, CompanyName) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateCRMClientRecord([FromBody]UpdateCRMClientRecord model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.LastModifiedBy = userid;
                return Ok(new { result = objCRMClientRecord.UpdateCrmClientRecord(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetReferralContactGrid(int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetReferralContactGrid(ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]
          public IActionResult DeleteClientRecord(int ClientID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.DeleteClientRecord(ClientID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]

        public IActionResult GetReferralContactById(int ClientReferralContactsId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetReferralContactById(ClientReferralContactsId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult SaveReferralContact([FromBody]SaveReferralContact model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.CreatedBy = userid;
                return Ok(new { result = objCRMClientRecord.SaveReferralContact(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateReferralContact([FromBody]UpdateReferralContact model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.LastModifiedBy = userid;
                return Ok(new { result = objCRMClientRecord.UpdateReferralContact(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
[HttpGet]
          public IActionResult DeleteReferralcontact(int ClientReferralContactsId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.DeleteReferralcontact(ClientReferralContactsId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]

        public IActionResult GetAccountContactGrid(int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetAccountContactGrid(ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]

        public IActionResult GetAccountContactById(int ClientAccountContactsId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMClientRecord.GetAccountContactById(ClientAccountContactsId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult SaveAccountContact([FromBody]SaveAccountContact model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.CreatedBy = userid;
                return Ok(new { result = objCRMClientRecord.SaveAccountContact(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateAccountContact([FromBody]UpdateAccountContact model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.LastModifiedBy = userid;
                return Ok(new { result = objCRMClientRecord.UpdateAccountContact(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]
          public IActionResult DeleteAccountcontact(int UserId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.DeleteAccountcontact(UserId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetNote(int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCRMClientRecord.GetNote(ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult SaveNote([FromBody]SaveNoteModel model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.NoteCreatedByUserId = userid;
                return Ok(new { result = objCRMClientRecord.SaveNote(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetClientServices(int ClientTypeId, int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetClientServices(ClientTypeId, ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpPost]
        public async Task<IActionResult> UploadDocument(int ClientId, [FromForm]UploadFile objUploadFile)
        {
            try
            {
                var context = HttpContext.Request;

                if (context.Form.Files.Count > 0)
                {
                    var postedFile = context.Form.Files[0];
                    string relativePath = _configuration["EnviromentVariables:FileFolder"].ToString();// "" + ConfigurationManager.AppSettings["FileFolder"].ToString() + "Documents/"  + "/" + objUploadFile.ClientId + "/";
                    string serverPath = _configuration["EnviromentVariables:FileServer"].ToString();// @"" + ConfigurationManager.AppSettings["FileServer"].ToString() + relativePath;

                    DateTime today = System.DateTime.Now;
                    string fileId = "" + today.Day + today.Month + today.Year + today.Hour + today.Minute + today.Second + "";
                    var filePath = serverPath + '/' + fileId;
                    relativePath = relativePath + '/' + fileId;
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    filePath = filePath + '/' + postedFile.FileName;
                    relativePath = relativePath + '/' + postedFile.FileName;

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await postedFile.CopyToAsync(stream);
                    }
                    objUploadFile.DocumentName = postedFile.FileName;
                    objUploadFile.DocumentPath = relativePath;
                    int userid = GetUserId();
                    objUploadFile.UploadedBy = userid;
                    int result = objCRMClientRecord.SaveDocument(objUploadFile);
                    return Ok(new { result = filePath, relativePath });
                }
                else
                    return StatusCode(500, "Request does not contains file.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        private string GetContentType(string path)
        {
            var provider = new FileExtensionContentTypeProvider();

            string contentType;
            FileInfo fi = new FileInfo(path);
            switch (fi.Extension.ToString())
            {
                case ".doc":
                    // Response.ClearContent();
                    Response.ContentType = "application/msword";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/msword";
                    }

                    break;
                case ".wav":
                    Response.ContentType = "application/octet-stream";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/octet-stream";
                    }
                    //Response.AddHeader("Content-Length", "304578");
                    break;
                case ".pdf":
                    Response.ContentType = "application/pdf";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/pdf";
                    }
                    //  Response.AddHeader("Content-Length", fi.Length.ToString());

                    break;
                case ".txt":
                    Response.ContentType = "text/plain";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "text/plain";
                    }
                    //  Response.AddHeader("Content-Length", fi.Length.ToString());
                    break;
                case ".tif":
                    Response.ContentType = "image/tiff";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "image/tiff";
                    }
                    break;
                case ".xls":
                    Response.ContentType = "application/vnd.ms-excel";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/vnd.ms-excel";
                    }
                    //   Response.Headers.Add("Content-Length", filePath.Length.ToString());
                    break;
                case ".xlsx":
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    }
                    //Response.Headers.Add("Content-Length", fi.Length.ToString());
                    break;
                default:
                    Response.ContentType = "application/octet-stream";
                    contentType = "application/octet-stream";
                    break;
                    //        }

            }
            return contentType;
        }

        [HttpGet]
        public async Task<IActionResult> DownloadDocument(int DocumentId)
        {
            try
            {
                string result = objCRMClientRecord.DownloadDocument(DocumentId);
                var folder = result.Split('/');
                string strDocumentPath = (_configuration["EnviromentVariables:FileFolder"].ToString() + "\\" + folder[1] + "\\" + folder[2]);
                if (!System.IO.File.Exists(strDocumentPath))
                    return NotFound();

                var memory = new MemoryStream();
                using (var stream = new FileStream(strDocumentPath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;

                return File(memory, GetContentType(strDocumentPath), result, true);
            }
            catch (Exception ex)
            {
                 return StatusCode(500, ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> ViewCRMDcument(int DocumentId)
        {
           try
            {
             string result = objCRMClientRecord.DownloadDocument(DocumentId);
             var folder = result.Split('/');
             result =(_configuration["EnviromentVariables:DocumentViewPath"].ToString() + folder[1] + "/" + folder[2]);
             return Ok(new{result});
            }
           catch (Exception ex)
            {
                return StatusCode(500, ex);
            }  
        }
       
        [HttpGet]

        public IActionResult GetClientDocuments(int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetClientDocuments(ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetRelationshipOwner()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetRelationshipOwner() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

                [HttpGet]

        public IActionResult GetBroker()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetBroker() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult SaveRelationShipOwner([FromBody]SaveRelationshipOwner model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.CreatedBy = userid;
                return Ok(new { result = objCRMClientRecord.SaveRelationShipOwner(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult SaveBroker([FromBody]SaveBroker model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.CreatedBy = userid;
                return Ok(new { result = objCRMClientRecord.SaveBroker(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]

        public IActionResult GetOwnerById(int OwnerId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetOwnerById(OwnerId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetBrokerById(int BrokerId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.GetBrokerById(BrokerId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateRelationShipOwner([FromBody]SaveRelationshipOwner model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.LastModifiedBy = userid;
                return Ok(new { result = objCRMClientRecord.UpdateRelationShipOwner(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }



        [HttpPost]
        public IActionResult UpdateBroker([FromBody]SaveBroker model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.LastModifiedBy = userid;
                return Ok(new { result = objCRMClientRecord.UpdateBroker(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult CheckCompanyName(string CompanyName, int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
           // CompanyName= Encryption.Encrypt(CompanyName);
         CompanyName= WebUtility.HtmlDecode (CompanyName);
          // CompanyName = Encryption.Decrypt(CompanyName);


            try
            {
                return Ok(new { result = objCRMClientRecord.CheckCompanyName(CompanyName, ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]
        public IActionResult CheckRealtionShipOwner(string Name)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.CheckRealtionShipOwner(Name) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        public IActionResult DeleteDocument(int DocumentId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                 int userid = GetUserId();
                 int LastModifiedBy=userid;
                string result = objCRMClientRecord.DownloadDocument(DocumentId);
                var folder = result.Split('/');
                //  string strDocumentPath = (_configuration["EnviromentVariables:FileFolder"].ToString() + "\\" + folder[1]);// ConfigurationManager.AppSettings["DocumentPath"];
                string strDocumentPath = (_configuration["EnviromentVariables:FileFolder"].ToString() + "\\" + folder[1] + "\\" + folder[2]);
                System.IO.File.Delete(strDocumentPath);

                return Ok(new { result = objCRMClientRecord.DeleteDocument(DocumentId, LastModifiedBy) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    [HttpGet]
        public IActionResult SearchNoteFromList(string NoteInput,int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCRMClientRecord.SearchNoteFromList(NoteInput,ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }



    }
}

