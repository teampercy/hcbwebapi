using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
//using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Controllers
{
   [ApiController]
   [Route("api/[controller]/[action]")]
    public class ClientReportController : ControllerBase
    {
        ClientReport objClientReport;
    
        IConfiguration _configuration;
         private int GetUserId()
         {
             return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
         }

        public ClientReportController(IConfiguration configuration, ClientReport CRMUserRecordayer)
        {
            _configuration = configuration;
            objClientReport = CRMUserRecordayer;

        }
     [HttpPost]
      public IActionResult GetCRMClientReportList([FromBody]GetClientReport model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objClientReport.GetClientReportList(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    [HttpGet]
        public IActionResult GetddlServiceList()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objClientReport.GetddlServiceList() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

  

    }
}