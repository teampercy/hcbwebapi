using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace HCBNewAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        Login login;
        private IConfiguration _config;
        public LoginController(IConfiguration config, Login loginlayer)
        {
            _config = config;
            login = loginlayer;
        }

        [AllowAnonymous]
        [HttpPost]
      
        public IActionResult Login([FromBody]UserModel login)
        {
            login.password = Encryption.Encrypt(login.password);
            IActionResult response = Unauthorized();
            var user = this.login.GetLoginUser(login);

            if (user.result == 0)
            {
                user.Token = GenerateJSONWebToken(user);
                response = Ok(new { user });
            }
            else if (user.result != 0)
            {
                response = BadRequest(new { user });
            }
            return response; 
           
        }

        private string GenerateJSONWebToken(UserModel userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                          new Claim(JwtRegisteredClaimNames.Sub, userInfo.username),
                          new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                          new Claim("UserId",Convert.ToString(userInfo.UserId))
                        };
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddHours(8),
             // expires: DateTime.Now.AddSeconds(5),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}