using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
// using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;

namespace HCBNewAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class CaseManagmentController : ControllerBase
    {
        CaseManagment objCaseManagment;
        IConfiguration _configuration;
        private int GetUserId()
        {
            return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
        }

        public CaseManagmentController(IConfiguration configuration, CaseManagment caseManagmentLayer)
        {
            _configuration = configuration;
            objCaseManagment = caseManagmentLayer;
        }

        // [HttpGet]

        // public IActionResult GetCaseManagmentList(int UserId, string usertype, bool Isopen)
        // {
        //     HttpResponseMessage response = new HttpResponseMessage();
        //     try
        //     {
        //         return Ok(new { result = objCaseManagment.GetCaseManagmentList(UserId, usertype, Isopen) });
        //     }
        //     catch (Exception ex)
        //     {
        //         return StatusCode(500, ex);
        //     }
        //     //return StatusCode(500);
        // }
       
        public IActionResult uploadSubscriberfile([FromBody] List<ImportCases> objImportFileList)
        {
             List<ImportResult>  result = new  List<ImportResult>();
           try{
               int UserId=GetUserId();
             result = objCaseManagment.ImportCaseManagmentList(objImportFileList,UserId) ;
            // if(result.Count>1)
            // {
           // return StatusCode(200, new { result });
            return Ok(new {result});
            // }
            // else{
            // return StatusCode(200, new { result });
            // }
           }
           catch(Exception ex)
           {
            return StatusCode(200, new { result });
           }
        }

        [HttpPost]

        public IActionResult GetCaseManagmentList([FromBody]GetHCBClientGrid model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetCaseManagmentList(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
            //return StatusCode(500);
        }

        [HttpGet]

        public IActionResult GetCaseInfoById(int HCBReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetCaseInfoById(HCBReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDdlPrimaryICD10Code()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetDdlPrimaryICD10Code() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]

        public IActionResult GetddlCaseManagment()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetddlCaseManagment() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetddlClient()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetddlClient() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetddlClientContacts(int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetddlClientContacts(ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

              [HttpGet]

        public IActionResult GetddlServiceTypeForEdit()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetddlServiceTypeForEdit() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetddlServiceType(int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetddlServiceType(ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetddlServiceRequired()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetddlServiceRequired() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        // public IActionResult GetddlTypelistValues(int TypeId)
        // {
        //     HttpResponseMessage response = new HttpResponseMessage();
        //     try
        //     {
        //         // int UserId=GetUserId();
        //         return Ok(new { result = objCaseManagment.GetddlTypelistValues(TypeId) });
        //     }
        //     catch (Exception ex)
        //     {
        //         return StatusCode(500, ex);
        //     }
        // }

        //       [HttpGet]
        // public IActionResult SendNewCaseEmail(string EmailAddress, int RefID)
        // {
        //     HttpResponseMessage response = new HttpResponseMessage();
        //     try
        //     {
        //         int userid = GetUserId();
        //        // model.SaveUpdateCaseManagmentModel.UserId = userid;
        //         return Ok(new { result = objCaseManagment.SendNewCaseEmail(EmailAddress,RefID) });
        //     }
        //     catch (Exception ex)
        //     {
        //         return StatusCode(500, ex);
        //     }
        // }

      [HttpPost]
        public IActionResult SaveCaseManagment([FromBody]SaveHCBViewModel model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.SaveUpdateCaseManagmentModel.UserId = userid;
                return Ok(new { result = objCaseManagment.SaveCaseManagment(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateCaseManagment([FromBody]SaveUpdateCaseManagmentModel model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.UserId = userid;
                return Ok(new { result = objCaseManagment.UpdateCaseManagment(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetTelephoneActivityList(int HCBReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetTelephoneActivityList(HCBReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetHCBTelephoneActivity(int HCBTelephoneID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetHCBTelephoneActivity(HCBTelephoneID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult SaveTelephoneActivity([FromBody]SaveTelephoneActivityViewModel model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.CreatedBy = userid;
                return Ok(new { result = objCaseManagment.SaveTelephoneActivity(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateTelephoneActivity([FromBody]SaveUpdateTelephoneActivity model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.LastModifiedBy = userid;
                return Ok(new { result = objCaseManagment.UpdateTelephoneActivity(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult DeleteTelephoneActivity(int HCBTelephoneID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.DeleteTelephoneActivity(HCBTelephoneID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetFailedCallList(int HCBTelephoneID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetFailedCallList(HCBTelephoneID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult SaveFailedCall([FromBody]SaveFailedCall model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.CreatedBy = userid;
                return Ok(new { result = objCaseManagment.SaveFailedCall(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult DeleteFailedCall(int FailedCallID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.DeleteFailedCall(FailedCallID) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetHCBVisitActivityGrid(int HCBReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetHCBVisitActivityGrid(HCBReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        [HttpGet]
        public IActionResult GetCaseActivityGrid(int HcbReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetCaseActivityGrid(HcbReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }

        }
         public IActionResult GetCaseActivityDetail(int CaseActivityId)
         {
              HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetCaseActivity(CaseActivityId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
  
         }

        [HttpPost]
        public IActionResult UpdateCaseActivity([FromBody]SaveUpdateCaseActivity model)
        {
                HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.UserId = userid;
                return Ok(new { result = objCaseManagment.UpdateCaseActivityv(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

       [HttpPost]
        public IActionResult  SaveCaseActivity([FromBody]SaveUpdateCaseActivity model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.UserId = userid;
                return Ok(new { result = objCaseManagment.SaveCaseActivity(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        
    [HttpGet]

        public IActionResult DeleteCaseActivity(int CaseActivityId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.DeleteCaseActivity(CaseActivityId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetHCBVisitActivity(int HCBVisitId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetHCBVisitActivity(HCBVisitId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult SaveHCBVisitActivity([FromBody]SaveUpdateHCBVisitActivity model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.CreatedBy = userid;
                return Ok(new { result = objCaseManagment.SaveHCBVisitActivity(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateHCBVisitActivity([FromBody]SaveUpdateHCBVisitActivity model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.LastModifiedBy = userid;
                return Ok(new { result = objCaseManagment.UpdateHCBVisitActivity(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

    [HttpGet]

        public IActionResult DeleteVisitActivity(int HCBVisitId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.DeleteVisitActivity(HCBVisitId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
 [HttpPost]
        public async Task<IActionResult> UploadHCBDocument(int ClientId, [FromForm]UploadHCBFile objUploadHCBFile)
        {
            try
            {
                var context = HttpContext.Request;

                if (context.Form.Files.Count > 0)
                {
                    var postedFile = context.Form.Files[0];
                    string relativePath = _configuration["EnviromentVariables:FileFolder"].ToString();// "" + ConfigurationManager.AppSettings["FileFolder"].ToString() + "Documents/"  + "/" + objUploadFile.ClientId + "/";
                    string serverPath = _configuration["EnviromentVariables:FileServer"].ToString();// @"" + ConfigurationManager.AppSettings["FileServer"].ToString() + relativePath;

                    DateTime today = System.DateTime.Now;
                    string fileId = "" + today.Day + today.Month + today.Year + today.Hour + today.Minute + today.Second + "";
                    var filePath = serverPath + '/' + fileId;
                    relativePath = relativePath + '/' + fileId;
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    filePath = filePath + '/' + postedFile.FileName;
                    relativePath = relativePath + '/' + postedFile.FileName;

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await postedFile.CopyToAsync(stream);
                    }
                    objUploadHCBFile.DocumentName = postedFile.FileName;
                    objUploadHCBFile.DocumentPath = relativePath;
                    int userid = GetUserId();
                    objUploadHCBFile.UploadedBy = userid;
                    int result = objCaseManagment.SaveHCBDocument(objUploadHCBFile);
                    return Ok(new { result = filePath, relativePath });
                }
                else
                    return StatusCode(500, "Request does not contains file.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        [HttpGet]
        public IActionResult GetImportedResultList()
        {
            
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetImportedResultList() });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SaveImoprtedDocument(int ClientId, [FromForm]UploadHCBFile objUploadHCBFile)
        {
             List<ImportResult>  result = new  List<ImportResult>();
              int UserId=GetUserId();
            try
            {
                var context = HttpContext.Request;

                if (context.Form.Files.Count > 0)
                {
                    var postedFile = context.Form.Files[0];
                    string relativePath = _configuration["EnviromentVariables:FileFolder"].ToString();// "" + ConfigurationManager.AppSettings["FileFolder"].ToString() + "Documents/"  + "/" + objUploadFile.ClientId + "/";
                    string serverPath = _configuration["EnviromentVariables:importFilePath"].ToString();// @"" + ConfigurationManager.AppSettings["FileServer"].ToString() + relativePath;

                    DateTime today = System.DateTime.Now;
                    string fileId = "" + today.Day + today.Month + today.Year + today.Hour + today.Minute +  today.Second+"";
                    var filePath = serverPath + '/' + fileId;
                    relativePath = relativePath + '/' + fileId;
                    if (!Directory.Exists(filePath))
                    {
                        Directory.CreateDirectory(filePath);
                    }
                    filePath = filePath + '/' + postedFile.FileName;
                    relativePath = relativePath + '/' + postedFile.FileName;

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await postedFile.CopyToAsync(stream);
                    }
                    objUploadHCBFile.DocumentName = postedFile.FileName;
                    objUploadHCBFile.DocumentPath = relativePath;
                    result = objCaseManagment.Import(UserId,filePath,true);
                  // return Ok(new {result});
                  //  return Ok(new { result = filePath, relativePath });
                }
                else
                    return StatusCode(500, "Request does not contains file.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
             return Ok(new {result});
        }
        private string GetContentType(string path)
        {
            var provider = new FileExtensionContentTypeProvider();

            string contentType;
            FileInfo fi = new FileInfo(path);
            switch (fi.Extension.ToString())
            {
                case ".doc":
                    // Response.ClearContent();
                    Response.ContentType = "application/msword";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/msword";
                    }

                    break;
                case ".wav":
                    Response.ContentType = "application/octet-stream";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/octet-stream";
                    }
                    //Response.AddHeader("Content-Length", "304578");
                    break;
                case ".pdf":
                    Response.ContentType = "application/pdf";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/pdf";
                    }
                    //  Response.AddHeader("Content-Length", fi.Length.ToString());

                    break;
                case ".txt":
                    Response.ContentType = "text/plain";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "text/plain";
                    }
                    //  Response.AddHeader("Content-Length", fi.Length.ToString());
                    break;
                case ".tif":
                    Response.ContentType = "image/tiff";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "image/tiff";
                    }
                    break;
                case ".xls":
                    Response.ContentType = "application/vnd.ms-excel";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/vnd.ms-excel";
                    }
                    //   Response.Headers.Add("Content-Length", filePath.Length.ToString());
                    break;
                case ".xlsx":
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    if (!provider.TryGetContentType(path, out contentType))
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    }
                    //Response.Headers.Add("Content-Length", fi.Length.ToString());
                    break;
                default:
                    Response.ContentType = "application/octet-stream";
                    contentType = "application/octet-stream";
                    break;
                    //        }

            }
            return contentType;
        }

        public IActionResult ViewDcument(int DocumentId)
        {
            try
            {
             string result = objCaseManagment.DownloadDocument(DocumentId);
             var folder = result.Split('/');
             result =(_configuration["EnviromentVariables:DocumentViewPath"].ToString() + folder[1] + "/" + folder[2]);
      return Ok(new{result});
            }
       catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]
        public async Task<IActionResult> DownloadDocument(int DocumentId)
        {
            try
            {
                string result = objCaseManagment.DownloadDocument(DocumentId);
                var folder = result.Split('/');
                string strDocumentPath = (_configuration["EnviromentVariables:FileFolder"].ToString() + "\\" + folder[1] + "\\" + folder[2]);
                if (!System.IO.File.Exists(strDocumentPath))
                    return NotFound();

                var memory = new MemoryStream();
                using (var stream = new FileStream(strDocumentPath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;

                return File(memory, GetContentType(strDocumentPath), result, true);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        public IActionResult DeleteDocument(int DocumentId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.LastModifiedBy=userid;
                string result = objCaseManagment.DownloadDocument(DocumentId);
                var folder = result.Split('/');
                //  string strDocumentPath = (_configuration["EnviromentVariables:FileFolder"].ToString() + "\\" + folder[1]);// ConfigurationManager.AppSettings["DocumentPath"];
                string strDocumentPath = (_configuration["EnviromentVariables:FileFolder"].ToString() + "\\" + folder[1] + "\\" + folder[2]);
                System.IO.File.Delete(strDocumentPath);

                return Ok(new { result = objCaseManagment.DeleteDocument(DocumentId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetHCBDocuments(int HCBReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetHCBDocuments(HCBReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetHCBNote(int HCBReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objCaseManagment.GetHCBNote(HCBReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult SaveHCBNote([FromBody]SaveHCBNote model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.LoggedInUserId = userid;
                return Ok(new { result = objCaseManagment.SaveHCBNote(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

       public IActionResult DeleteHCBNote(int Id)
       {
         HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();

                return Ok(new { result = objCaseManagment.DeleteNote(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
       }


       [HttpGet]
        public IActionResult SendEmail(bool IsImportant,int usertypeId,string NoteCreatedDate,string HCBReferenceNo, int UserId,int CanWorkAsCM)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                
                return Ok(new { result = objCaseManagment.SendEmail(IsImportant,usertypeId,NoteCreatedDate,HCBReferenceNo, UserId,CanWorkAsCM) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult SaveSubsequentActivity([FromBody]SaveUpdateSubsequentActivity model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.UserId = userid;
                return Ok(new { result = objCaseManagment.SaveSubsequentActivity(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateSubsequentActivity([FromBody]SaveUpdateSubsequentActivity model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                int userid = GetUserId();
                model.UserId = userid;
                return Ok(new { result = objCaseManagment.UpdateSubsequentActivity(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetSubsequentActivityGrid(int HCBReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetSubsequentActivityGrid(HCBReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetSubsequentActivity(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetSubsequentActivity(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult DeleteSubsequentActivity(int Id)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.DeleteSubsequentActivity(Id) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetAnnualReportSummaryGrid(int HCBReferenceId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetAnnualReportSummaryGrid(HCBReferenceId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetAnnualReportSummaryById(int AnnualReportId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.GetAnnualReportSummaryById(AnnualReportId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult SaveAnnualReportSummary([FromBody]SaveUpdateAnnualReportSummary model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.CreatedBy = userid;
                return Ok(new { result = objCaseManagment.SaveAnnualReportSummary(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpPost]

        public IActionResult UpdateAnnualReportSummary([FromBody]SaveUpdateAnnualReportSummary model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int userid = GetUserId();
                // model.LastModifiedBy = userid;
                return Ok(new { result = objCaseManagment.UpdateAnnualReportSummary(model) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

             [HttpGet]

        public IActionResult DeleteAnnualReportSummary(int AnnualReportId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.DeleteAnnualReportSummary(AnnualReportId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                    [HttpGet]

        public IActionResult CheckClaimantExistOrNot(string FirstName, string LastName, string BirthDate,int HCBRefId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.CheckClaimantExistOrNot(FirstName, LastName, BirthDate,HCBRefId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

      [HttpGet]

        public IActionResult SaveCaseCancelReason(int HCBRefId, string Reason)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                 int userid = GetUserId();
                return Ok(new { result = objCaseManagment.SaveCaseCancelReason(HCBRefId, Reason,userid)});
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

  [HttpGet]
        public IActionResult SearchHCBNote(string NoteInput,int ClientId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objCaseManagment.SearchHCBNote(NoteInput,ClientId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        
    }
}