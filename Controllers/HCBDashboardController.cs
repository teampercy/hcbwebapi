using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Linq;
using HCBNewAPI.HCBBLL;
using HCBNewAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
// using System.Web.Http;
using System.Security.Claims;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]/[action]")]
    public class HCBDashboardController : ControllerBase
    {
        HCBDashboard objHCBDashboard;
        IConfiguration _configuration;
        private int GetUserId()
        {
            return int.Parse(this.User.Claims.Where(i => i.Type == "UserId").Select(i => i.Value).FirstOrDefault());
        }

        public HCBDashboardController(IConfiguration configuration, HCBDashboard HCBDashboardlayer)
        {
            _configuration = configuration;
            objHCBDashboard = HCBDashboardlayer;
        }

        [HttpGet]

        public IActionResult GetDashboardTopFiveAssessorList(int userId,string usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                // var result = objHCBDashboard.GetDashboardTopFiveAssessorList(userId,usertype);
                // return Ok(new { result=(result==[] ?0:result) });
                return Ok(new{result = objHCBDashboard.GetDashboardTopFiveAssessorList(userId,usertype)});
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardTopFiveSchemeList(int userId,string usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetDashboardTopFiveSchemeList(userId,usertype) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetddlBrokerList(int userId,string usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetddlBrokerList(userId,usertype) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetddlSchemeList(int userId,string usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetddlSchemeList(userId,usertype) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetScheme(int UserId,string UserType,bool IsOpen,int SchemeId,string SchemeStartDate,string SchemeEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetScheme(UserId,UserType,IsOpen,SchemeId,SchemeStartDate,SchemeEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]

        public IActionResult GetDashboardAverageRTWDuration(int UserId,string UserType,int RTWclientId,int RTWClientTypeId,string RTWStartDate,string RTWEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetDashboardAverageRTWDuration(UserId,UserType,RTWclientId,RTWClientTypeId,RTWStartDate,RTWEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardClaimantAverageAge(int UserId,string UserType,int AssessorAgeId,int SchemeAgeId,string AgeStartDate,string AgeEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetDashboardClaimantAverageAge(UserId,UserType,AssessorAgeId,SchemeAgeId,AgeStartDate,AgeEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardReturnToWorkReason(int userId,string usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetDashboardReturnToWorkReason(userId,usertype) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardReasonForReferal(int userId,string usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                // int UserId=GetUserId();
                return Ok(new { result = objHCBDashboard.GetDashboardReasonForReferal(userId,usertype) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


        [HttpGet]

        public IActionResult GetHCBDashboardTotalValues(int userId,string usertype)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetHCBDashboardTotalValues(userId, usertype) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
   [HttpGet]
        public IActionResult GetListForTotalValues(int Index, int userId,string usertype, int ClientTypeId)
            {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetListForTotalValues(Index,userId, usertype, ClientTypeId) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


 [HttpGet]

        public IActionResult GetDashboardInterventionDuration(int UserId,string UserType,int InterventionSectorId,int InterventionClientId,string InterventionStartDate,string InterventionEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardInterventionDuration(UserId,UserType,InterventionSectorId,InterventionClientId,InterventionStartDate,InterventionEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardClaimantGender(int UserId,string UserType,int typeId,int ClientId,string ClaimantGenderStartDate,string ClaimantGenderEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardClaimantGender(UserId,UserType,typeId,ClientId,ClaimantGenderStartDate,ClaimantGenderEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

         [HttpGet]

        public IActionResult GetDashboardClosedCaseAnalysis(int UserId,string UserType,int typeId,int ClientId,string ClosedCaseStartDate,string ClosedCaseEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardClosedCaseAnalysis(UserId,UserType,typeId,ClientId,ClosedCaseStartDate,ClosedCaseEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

        [HttpGet]

        public IActionResult GetDashboardReasonReferalGrid(int UserId,string UserType,int RFRTypeId,int RFRClientId,string ReasonReferalStartDate,string ReasonReferalEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardReasonReferalGrid(UserId,UserType,RFRTypeId,RFRClientId,ReasonReferalStartDate,ReasonReferalEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

       [HttpGet]

        public IActionResult GetClientBySectorGrid(int clientTypeId,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetClientBySectorGrid(clientTypeId,UserId,UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

              [HttpGet]
        public IActionResult GetCaseActivityDefinedByWeek(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetCaseActivityDefinedByWeek(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
       [HttpGet]
        public IActionResult BPSByDefinedDates(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.BPSByDefinedDates(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
               [HttpGet]
        public IActionResult SECBPSByDefinedDates(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.SECBPSByDefinedDates(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
  [HttpGet]
        public IActionResult GetBPSDetails(int UserId,string UserType,string RFRTypeId,string RFRClientId,string ReasonReferalStartDate,string ReasonReferalEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetBPSDetails(UserId,UserType, RFRTypeId,RFRClientId, ReasonReferalStartDate, ReasonReferalEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

         [HttpGet]
        public IActionResult GetSECBPSDetails(int UserId,string UserType,string RFRTypeId,string RFRClientId,string ReasonReferalStartDate,string ReasonReferalEndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetSECBPSDetails(UserId,UserType, RFRTypeId,RFRClientId, ReasonReferalStartDate, ReasonReferalEndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }


              [HttpGet]
        public IActionResult CaseCloseReasonOutcome(int UserId,string UserType,string StartDate,string EndDate)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.CaseCloseReasonOutcome(UserId, UserType, StartDate, EndDate) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                [HttpGet]
        public IActionResult ActivityByDefinedDateMaster(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.ActivityByDefinedDateMaster(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }

                [HttpGet]
        public IActionResult GetDashboardAreaChartOPN(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardAreaChartOPN(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                [HttpGet]
        public IActionResult GetDashboardAreaChartCLS(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardAreaChartCLS(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                [HttpGet]
        public IActionResult GetDashboardAreaChartOPNI(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardAreaChartOPNI(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                [HttpGet]
        public IActionResult GetDashboardAreaChartOPNR(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardAreaChartOPNR(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                [HttpGet]
        public IActionResult GetDashboardAreaChartCLSI(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardAreaChartCLSI(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
                [HttpGet]
        public IActionResult GetDashboardAreaChartCLSR(int WeekData,int UserId,string UserType)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                return Ok(new { result = objHCBDashboard.GetDashboardAreaChartCLSR(WeekData,UserId, UserType) });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}