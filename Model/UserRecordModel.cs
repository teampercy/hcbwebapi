using System;

namespace HCBNewAPI.Models
{
    public class UserRecordModel
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public string sort { get; set; }
        public string direction { get; set; }
        public string FullName { get; set; }
        public string UserTypeId {get; set;}
         public Boolean CanWrokAsCM {get;set;}
    }


    // public class UpdatecrmUserRecord
    // {
    //     public int UserID { get; set; }
    //     public string EmailAddress { get; set; }
    //     public string Password { get; set; }
    //     public string UserType { get; set; }
    //     public string FullName { get; set; }
    //     public string TelephoneNumber { get; set; }
    //     public string Address { get; set; }
    //     public int AccountActive { get; set; }
    //     // public int IncoID {get; set;}
    //     // public int TermsAccepted {get; set;}
    //     //public int NominatedNurseID {get; set;}
    //     public DateTime LastLoginDate { get; set; }
    //     public int FailedAttempt { get; set; }
    //     // public DateTime PasswordExpiryDate {get; set;}
    //     public string City { get; set; }
    //     public int IsOnline { get; set; }
    //     public int UserTypeId { get; set; }

    // }

    public class AddUpdatecrmUser
    {
        public int UserID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public string FullName { get; set; }
        public string TelephoneNumber { get; set; }
        public string Address { get; set; }
        public int AccountActive { get; set; }
        //public int IncoID { get; set; }
       // public int TermsAccepted { get; set; }
       // public int NominatedNurseID { get; set; }
        public int FailedAttempt { get; set; }
        public string City { get; set; }
        public int IsOnline { get; set; }
        public int UserTypeId { get; set; }
        public int CreatedBy {get; set;}
        public int ModifiedBy {get; set;}
         public Boolean CanWrokAsCM {get;set;}
         public int? SPComapny {get;set;}
    }
}