using System;

namespace HCBNewAPI.Models
{
   public class GetClientReport
    {
           public int pageIndex { get; set; }
        public int pageSize { get; set; }

        public string sort { get; set; }

        public string direction { get; set; }

        public string CompanyName { get; set; }

        public string ClientTypeId { get; set; }

        public string ClientCreatedDateFrom { get; set; } 
        public string export {get; set;}

        public  string ClientCreatedDateTo { get; set;}
        public string PrincipalContactEmail{get;set;}
      public int? RelationShipOwnerId {get; set;}
      public int? RegionId {get;set;}
       public string ClientServices { get; set;}
      public int? IsActive { get; set;}

    }
}