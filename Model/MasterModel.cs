using System;

namespace HCBNewAPI.Models
{
    public class MasterModel
    {
       public int Id{get;set;}
       public int TypeId{get;set;}
       public string Name{get;set;}
       public string RelatedValue{get;set;}

    }
    public class UpdateAssesor
    {
        public int AssesorId{get; set;}
        public string AssesorName{get; set;}
        public string TeamPhoneNo{get; set;}
        public string TeamEmailID{get; set;}
    }
public class AddEditSpCompany
    {
        public int SPCompanyId{get; set;}
        public string SPCompanyName{get; set;}
    }
}