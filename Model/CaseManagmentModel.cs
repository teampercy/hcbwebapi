using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Models
{

public class ListImportResult
{
   public List<ImportResult>  Listresult{get;set;}
}
public class ImportResult
{
    public string ClaimantFullName{get;set;}
    public string errorMessage{get;set;}
}

   public class ImportCases
   {
      public string Address_1 {get;set;} 
      public string Address_2 {get;set;} 
      public string City_Town {get; set;}
      public string Postcode {get; set;}
      public string Any_special_instruction {get; set;}
      public string Reason_for_absence {get; set;}
      public string CLIENT {get;set;}
      public string Landline{get;set;}
       public string Mobile{get;set;}
      public string DOB {get;set;}
   
public string Date_of_absence {get;set;}
public string Date_referred {get;set;}
        public string GIP {get;set;}
              public string GPE {get;set;}
              public string New_claim {get;set;}
           
  public string First_name {get;set;}
  public string Last_name {get;set;}
public string Scheme{get;set;}
public string SERVICE {get; set;}
public string f2f_Telephone {get; set;}
public string  CASEOWNER {get; set;}
   }

    public class CaseManagmentModel
    {
         public int CaseMgmtId
        { get; set; }

        public int HCBReference
        { get; set; }

        public int CaseMgmtCostId
        { get; set; }
        public decimal FundingLimit
        { get; set; }
        public bool ApprovedBy { get; set; }
        public string CaseDate { get; set; }
        public decimal CaseHours { get; set; }
        public decimal CaseRate { get; set; }
        public decimal CaseCost { get; set; }
    }

    public class GetHCBClientGrid
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }

        public string sort { get; set; }

        public string direction { get; set; }

        public string HCBReferenceNo{get;set;}

        public int? ClientId {get;set;}

        public string ClaimantDOB {get;set;}

       // public string ClientContact {get;set;}
public int? AssessorId {get; set;}
        public int? CaseMgrId{get;set;}

        public string ClaimantFirstName{get;set;}

        public string ClaimantLastName{get;set;}
        
        public string HCBReceivedDate{get;set;}

        public string usertype { get; set; }

        public int? UserId { get; set; }

        public int Isopen{get;set;}

    }

    // public class SaveCaseManagmentModel
    // {
    //     public string HCBReferenceNo { get; set; }			
    //     public int? ClientID { get; set; }					
    //     public int? CaseMgrId { get; set; }					
    //     public int? ClientContactId { get; set; }
    //     public string HCBReceivedDate{ get; set; }
    //     public string DateReferredToCaseOwner{get;set;}	
    //     public int? ServiceType{get;set;}		
    //     public string ClaimantFirstName { get; set; }			
    //     public string ClaimantLastName { get; set; }			
    //     public string ClaimantDOB { get; set; }				
    //     public string ClaimantGender { get; set; }			
    //     public string ClaimantPhone { get; set; }				
    //     public string ClaimantMobile { get; set; }			
    //     public string ClaimantOccupation{ get; set; }		
    //     public int? ClaimantServiceReq { get; set; }
    //     public string ClaimantAddress1 { get; set; }
    //     public string ClaimantAddress2 { get; set; }
    //     public string ClaimantCity { get; set; }
    //     public string ClaimantCounty{ get; set; }	
    //     public string EmployerName{ get; set; }
    //     public string EmployerContact{ get; set; }
    //     public string EmployerEmail	{ get; set; }
    //     public string ClientClaimRef{ get; set; }
    //     public int? IncapacityDefinition{get;set;}
    //     public int? SchemeId	{ get; set; }
    //     public int? DeferredPeriod{ get; set; }
    //     public string EndOfBenefitDate{ get; set; }
    //     public int? MonthlyBenefit{ get; set; }
    //     public bool IsEligibleforEAP{ get; set; }
    //     public string SchemeNumer{ get; set; }
    //     public string FirstAbsentDate{ get; set; }
    //     public int? IllnessInjuryId{ get; set; }
    //     public string DisabilityType{ get; set; }
    //    // public string HCBGroupInstructedDate{ get; set; }
    //     public string ClaimClosedDate{ get; set; }
    //     public int? ClaimClosedReasonId{ get; set; }
    //     public string ClaimDetails{ get; set; }
    //     public string SentToNurseDate{ get; set; }
    //     public string ClaimantFirstContactedDate{ get; set; }
    //     //public string FirstHRContactedDate{ get; set; }
    //     public string FirstVerbalContactClaimantDate{ get; set; }
    //     public int? TypeOfContactId{ get; set; }
    //     public string TypeOfContactDetails{ get; set; }
    //     public string NextReviewDate{ get; set; }
    //     public string FeeCharged{ get; set; }
    //     public string RehabCost	{ get; set; }
    //     public string DateCancelled	{ get; set; }
    //     public string ReturnToWorkDate{ get; set; }
    //     public string ReturnToWorkReason{ get; set; }
    //     public int? CaseClosedReasonId{get;set;}
    //     public string CaseClosedDate{get;set;}
    //     public int CreatedBy{get;set;}
    // }

    public class SaveUpdateCaseManagmentModel
    {
        public int HCBReferenceId {get;set;}
        public string HCBReferenceNo { get; set; }			
        public int? ClientID { get; set; }					
        public int? CaseMgrId { get; set; }
        public string HCBReceivedDate{ get; set; }				
        public int? ClientContactId { get; set; }
        public string DateReferredToCaseOwner{get;set;}		
        public int? ServiceType{get;set;}			
        public string ClaimantFirstName { get; set; }			
        public string ClaimantLastName { get; set; }			
        public string ClaimantDOB { get; set; }				
        public string ClaimantGender { get; set; }			
        public string ClaimantPhone { get; set; }				
        public string ClaimantMobile { get; set; }			
        public string ClaimantOccupation{ get; set; }		
        public int? ClaimantServiceReq { get; set; }
        public string ClaimantAddress1 { get; set; }
        public string ClaimantAddress2 { get; set; }
        public string ClaimantCity { get; set; }
        public string ClaimantCounty{ get; set; }	
        public int? OccupationalClassification{get;set;}
        public string EmployerName{ get; set; }
        public string EmployerContact{ get; set; }
        public string EmployerEmail	{ get; set; }
        public string ClientClaimRef{ get; set; }
        public int? DisabilityCategoryId{get;set;}
        public string DisabilityDescription{get;set;}
        public int? IncapacityDefination{get;set;}
        public int? SchemeId	{ get; set; }
        public int? DeferredPeriod{ get; set; }
        public string EndOfBenefitDate{ get; set; }
        public int? MonthlyBenefit{ get; set; }
        public bool? IsEligibleforEAP{ get; set; }
        public string SchemeNumer{ get; set; }
        public string FirstAbsentDate{ get; set; }
        public int? IllnessInjuryId{ get; set; }
        public string DisabilityType{ get; set; }
        public string ClaimClosedDate{ get; set; }
        public int? ClaimClosedReasonId{ get; set; }
        public string ClaimDetails{ get; set; }
        public string SentToNurseDate{ get; set; }
        public string ClaimantFirstContactedDate{ get; set; }
        public string FirstVerbalContactClaimantDate{ get; set; }
        public int? TypeOfContactId{ get; set; }
        public string TypeOfContactDetails{ get; set; }
        public string NextReviewDate{ get; set; }
        public string FeeCharged{ get; set; }
        public string RehabCost	{ get; set; }
        public string DateCancelled	{ get; set; }
        public string ReturnToWorkDate{ get; set; }
        public string ReturnToWorkReason{ get; set; } 
        public int? CaseClosedReasonId{get;set;}
       public int? PDAID {get;set;}
        public string CaseClosedDate{get;set;}   
        public string ClientRefEmail {get;set;}
        public string ClientRefTelephone {get;set;}
        public string DateFirstContactApplicant{get;set;}
        public string DateClaimFormSent{get;set;}
        public string ClaimantPostcode{get;set;}
        public string SubsequentActivity{get;set;}
        public string ServiceCaseClosed{get;set;}
        public string Outcome{get;set;}
        public string ReasonForDeclinature{get;set;}  
        public string SubmitedTraceSmart{get;set;}
        public string DataReturned{get;set;}
        public string PositiveTraceAge{get;set;}
        public string ReportSubmitted{get;set;}
        public string DateProjectClosed{get;set;}
        public string OverallTraces{get;set;}
        public string AnnualReportDate{get;set;}
        public string AnnualReportOutcome{get;set;}
        public int? AnnualReportValue{get;set;}
        public string HCBAppointedDate{get;set;}
        public int? NameOfTrust{get;set;}
        public int? LiveCoveredNumber{get;set;}
        public string ReportingIntervals{get;set;}
        public int? Assessor{get;set;}
        public int? AssessorRate{get;set;}
        public int? ClientRate{get;set;}
        public string ContractStartDate{get;set;}
        public string ContractEndDate{get;set;}
        public string SubmitedToProvider{get;set;}
        public string ReportReceived{get;set;}

        public string ReportDueBy{get;set;}
        public string ReportClient{get;set;}
        public string PensionSchmName {get;set;}
        public int? ServiceProviderId {get;set;}
        public string TimeSpent {get;set;}
        public int?  FeesPayable {get;set;}
        public int UserId{get;set;}
         public string DateofAssessment {get; set;}
         public int ReopenStatus {get; set;}

          public string EmployerTeleNo {get; set;}
          public string EmployerContact1{ get; set; }
          public string EmployerContact2{ get; set; }
         public int? ServiceRequestedId {get;set;}
public string Old_HCBRefId {get; set;}
            public string ClaimantTitle{get;set;}    //13/10/2020
   public string ClaimantEmail{get;set;}
   public string EmployerAddress{get;set;}
   public string EmployerOccupation{get;set;}
    public int? PDICDCode{get;set;}
    public string TimeLag{get;set;}
     public int? ClaimStatusId{get;set;}
   public string ClaimStatusDate{get;set;}
    public int? OutcomesId{get;set;}
  public string DateOfActivity{get;set;}
  public int? ClaimAssessorId{get;set;}
  public int? ActionId{get;set;}
     public string ReferrerName {get;set;}
     public string ReferrerEmail {get;set;}
     public string ReferrerTele {get;set;}
    }

    public class SaveUpdateTelephoneActivity
    {
        public int? HCBTelephoneID{get;set;}
        public int HCBReferenceId{get;set;}
        public string CallDate {get;set;}
        public string CallTime {get;set;}
        public int? TypeofCall {get;set;}
        public string CallExpectRTWDateOptimum{get;set;}
        public string CallExpectRTWDateMaximum {get;set;}
        public string CallCaseMngtRecom {get;set;}
        public int? FeeCharged {get; set;}
    }

    // public class UpdateTelephoneActivity
    // {
    //     public int HCBTelephoneID{get;set;}
    //     public int HCBReferenceNo{get;set;}
    //     public string CallDate {get;set;}
    //     public string CallTime {get;set;}
    //     public int TypeofCall {get;set;}
    //     public string CallExpectRTWDateOptimum{get;set;}
    //     public string CallExpectRTWDateMaximum {get;set;}
    //     public string CallCaseMngtRecom {get;set;}
    //     public string CallNxtSchedule {get;set;}
    //     public int FeeCharged {get; set;}    
    // }
    public class SaveFailedCall
    {
        public int HCBReferenceId {get; set;}
        public string failCallTime {get; set;}
        public string failCallDate {get; set;}
      //  public string call{get;set;}
      //  public string FailedCallDuration {get;set;}
        public int? HCBTelephoneID {get; set;}
    }

    public class SaveTelephoneActivityViewModel
    {
        public SaveUpdateTelephoneActivity saveUpdateTelephoneActivity {get; set;}
        public IEnumerable<SaveFailedCall> saveFailedcall {get; set;}
    }

    public class SaveUpdateHCBVisitActivity
    {
        public int HCBVisitId{get;set;}
        public int HCBReferenceId{get;set;}
        public string AppointmentDate{get;set;}
        public string ReportCompDate{get;set;}
        public decimal FeeChargedVisit{get;set;}
        public int? TYpeOfVisit{get;set;}
        // public int UserId {get;set;}
    }


    public class SaveUpdateAnnualReportSummary
    {
        public int AnnualReportId{get;set;}
        public int HCBReferenceId{get;set;}
        public string AnnualReportDate{get;set;}
        public string AnnualReportOutcome{get;set;}
        public string AnnualReportValue{get;set;}
    }


    public class UploadHCBFile
    {
        public int HCBReferenceId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public int UploadedBy { get; set; }
    }

    public class SaveHCBNote
    {
        public int HCBReferenceId{get;set;}
        public string NoteText{get;set;}
        public int LoggedInUserId{get;set;}
        public string EmailSentTo1{get;set;}
        public string EmailSentTo2{get;set;}
        public string EmailSentTo3{get;set;}
        public string EmailError {get;set;}
        public string comment {get;set;}
        public bool IsEmailSent {get;set;}
        public DateTime notecreatedDate {get; set;}
        
    }

    public class SaveUpdateSubsequentActivity
    {
        public int Id{get;set;}
        public int HCBReferenceId{get;set;}
        public string SubsequentActivityName{get;set;}
        public bool ReminderCheck{get;set;}
        public string ReminderDate{get;set;}
        public int UserId{get;set;}
    }

public class SaveUpdateCaseActivity
{
     public int UserId {get;set;}
  public int CaseActivityId {get;set;}
   public int HCBReferenceId  {get;set;}
  public string  DateOfActivity  {get;set;}
  public int? ClaimAssessorId {get;set;}
  public int?  ActionId {get;set;}
  public int?  ServiceProviderId {get;set;}
   public string ReportDueBy {get;set;}
  
}

            public class SaveHCBViewModel{    
        public SaveUpdateCaseManagmentModel SaveUpdateCaseManagmentModel { get; set; }
        // public IEnumerable<SaveReferralContact> saveReferralContact { get; set; }
        // public IEnumerable<SaveAccountContact> saveAccountContact{get;set;}
        public IEnumerable<SaveHCBNote> SaveHCBNote{get;set;}
    }

}
