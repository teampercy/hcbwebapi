using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace HCBNewAPI.Models
{
    public class GetCRMClientRecordGrid
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }

        public string sort { get; set; }

        public string direction { get; set; }

        public string CompanyName { get; set; }

        public int? ClientTypeId { get; set; }

        public string clientAddedDate { get; set; }
    }


    public class SaveCRMClientRecord
    {
        public DateTime clientAddedDate { get; set; }
        public int ClientTypeId { get; set; }
        public string CompanyName { get; set; }
        public string HeadOfficeAddress { get; set; }
          public int? RegionId {get; set;}
        public string Town {get; set;}
        public string Postcode {get; set;}
        public string LandlineNo {get; set;}
        public string MobileNo {get; set;}
        public string PrincipalContactName { get; set; }
        public string PrincipalContactNo { get; set; }
        public string PrincipalContactEmail { get; set; }
        public int? BrokerId { get; set; }
        public string PensionScheme { get; set; }
        public bool FinalSalary { get; set; }
        public bool IllHealthEarlyRetirement { get; set; }
        public string IPInsurerName { get; set; }
        public string DeferredPeriod { get; set; }
        public string Duration { get; set; }
        public string RenewalDue { get; set; }
        public string DefinitionOfOccupation { get; set; }
        public string MIInsurerAdminName { get; set; }
        public string CPInsurerName { get; set; }
        public string EAPProviderName { get; set; }
        public int? DaysPaidSickLeave { get; set; }
        public string OHPprovider { get; set; }
        public string Recording { get; set; }
        public int? RelationshipOwner { get; set; }
        public string MeetingIntervals { get; set; }
        public string FormalContractReqd { get; set; }
        public string ContractSignedDate { get; set; }
        public string ContractExpiryDate { get; set; }
        public bool ConsentToUseLogo { get; set; }
        public bool Status { get; set; }
        public int CreatedBy { get; set; }
        public string ClientServices{get;set;}
    public bool SendNotificationEmail{get;set;}
   public string UserName{get;set;}

   public int RefSourceId{get;set;}

    }

    public class UpdateCRMClientRecord
    {
        // public DateTime clientAddedDate { get; set; }
        public int ClientID { get; set; }
        public int ClientTypeId { get; set; }
        public string CompanyName { get; set; }
        public int? RegionId {get; set;}
        public string HeadOfficeAddress { get; set; }
        public string Town {get; set;}
        public string Postcode {get; set;}
         public string LandlineNo {get; set;}
        public string MobileNo {get; set;}
        public string PrincipalContactName { get; set; }
        public string PrincipalContactNo { get; set; }
        public string PrincipalContactEmail { get; set; }
        public int? BrokerId { get; set; }
        public string PensionScheme { get; set; }
        public bool FinalSalary { get; set; }
        public bool IllHealthEarlyRetirement { get; set; }
        public string IPInsurerName { get; set; }
        public string DeferredPeriod { get; set; }
        public string Duration { get; set; }
        public string RenewalDue { get; set; }
        public string DefinitionOfOccupation { get; set; }
        public string MIInsurerAdminName { get; set; }
        public string CPInsurerName { get; set; }
        public string EAPProviderName { get; set; }
        public int DaysPaidSickLeave { get; set; }
        public string OHPprovider { get; set; }
        public string Recording { get; set; }
        public int? RelationshipOwner { get; set; }
        public string MeetingIntervals { get; set; }
        public string FormalContractReqd { get; set; }
        public string ContractSignedDate { get; set; }
        public string ContractExpiryDate { get; set; }
        public bool ConsentToUseLogo { get; set; }
        public bool Status { get; set; }
        public int LastModifiedBy { get; set; }
       public string ClientServices{get;set;}
       public string NextActionDate {get; set;}

       public int RefSourceId{get;set;}
    }

     public class SaveUpdateClientProcessInfo
     {
          public int? ID{get;set;}
         public int? ClientId {get;set;}
         public string DateOfInformation {get;set;}
         public int? AgreedSLAId {get;set;} 
         public string  DefinedSLA {get;set;}
         public int? ReportingFormat {get;set;}
         public string  EncryptedEmailAddress {get;set;}
         public int? IsTLSEnabled {get;set;}
         public string  TLSDate {get;set;}
         public string  EncryptedPassword {get;set;}
         public int? InvoiceFrequency {get;set;}
         public string  SpecificInstruction {get;set;}
             public int? LastModifiedBy { get; set; }
             public int?   IsPurchesOrderRequired {get; set;}
       public string PONumber {get;set;}
       public string DatePOExpiry {get;set;}
                
     }

public class ReportEmailList
{
     public int? id {get; set;}
    public int? ClientId {get;set;}
public string emailAddress{get;set;}
public bool checkEmailAddress{get;set;}
   
}

    public class SaveReferralContact
    {
       
        public int? ClientId { get; set; }
        public string ContactName { get; set; }
        public string JobTitle { get; set; }
        public string PhoneNo { get; set; }
        public string NonHOLocation { get; set; }
        public string ContactEmail { get; set; }
        public int CreatedBy { get; set; }
    }

    public class UpdateReferralContact
    {
        public int ClientReferralContactsId { get; set; }
        public int ClientId { get; set; }
        public string ContactName { get; set; }
        public string JobTitle { get; set; }
        public string PhoneNo { get; set; }
        public string NonHOLocation { get; set; }
        public string ContactEmail { get; set; }
        public int LastModifiedBy { get; set; }
    }


    public class SaveAccountContact
    {
        public int clientId { get; set; }
        public string fullName { get; set; }
        public string emailAddress { get; set; }
        public string telephoneNumber { get; set; }
        public string instructions { get; set; }
        public string PaymentTerms { get; set; }
        public int CreatedBy { get; set; }
    }

    public class UpdateAccountContact
    {
        public int ClientAccountContactsId { get; set; }
        public int ClientId { get; set; }
        public string fullName { get; set; }
        public string emailAddress { get; set; }
        public string telephoneNumber { get; set; }
        public string Instructions { get; set; }
        public string PaymentTerms { get; set; }
        public int LastModifiedBy { get; set; }
    }

    public class UploadFile
    {
        // public int AttachmentParentID { get; set; }

        // public int AttachmentParentTypeID { get; set; }
        // public List<Attachments> ObjAttachments { get; set; }
        public int ClientId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public int UploadedBy { get; set; }
    }

    public class SaveNoteModel
    {
        public int ClientId { get; set; }
        public int NoteCreatedByUserId { get; set; }
        public string description{ get; set; }
        public string NextActionDate {get; set;}   

    }

    public class SaveRelationshipOwner
    {
        public int OwnerId{get;set;}
        public string OwnerName{get;set;}
        public string EmailAddress{get;set;}
        public int CreatedBy{get;set;}
        public int LastModifiedBy{get;set;}
    }

    public class SaveBroker
    {
        public int? BrokerId{get;set;}
        public string BrokerName{get;set;}
        public string EmailAddress{get;set;}
        public int CreatedBy{get;set;}
        public int LastModifiedBy{get;set;}
    }


    public class SaveContactViewModel{    
        public SaveCRMClientRecord saveCRMClientRecord { get; set; }
        public IEnumerable<SaveReferralContact> saveReferralContact { get; set; }
        public IEnumerable<SaveAccountContact> saveAccountContact{get;set;}
        public IEnumerable<SaveNoteModel> saveNoteModel{get;set;}
    }

       public class SaveUpdateClientProcessInfoViewModel{   
           public  IEnumerable<ReportEmailList> ReportEmailList {get;set;} 
        public SaveUpdateClientProcessInfo  SaveUpdateClientInfo { get; set; }
    }
    // public class SaveAccountContactViewModel{
    //     public SaveCRMClientRecord saveCRMClientRecord{get;set;}
        
    // }
}