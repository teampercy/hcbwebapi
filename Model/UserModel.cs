using System;

namespace HCBNewAPI.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string username
        { get; set; }

        public string password
        { get; set; }
        public string UserType
        { get; set; }

        public int UserTypeId
        { get; set; }

        public string Name
        { get; set; }

        public string PhoneNum
        { get; set; }

        public string Address
        { get; set; }

        public string City
        { get; set; }

        public int AccountActive
        { get; set; }

        public int ClientId
        { get; set; }

        public int TermsAccepted
        { get; set; }

        public int NominatedNurseId
        { get; set; }

        public int FaildAttempt
        { get; set; }

        public DateTime PasswordExpiryDate
        { get; set; }
        public string LastLogin
        { get; set; }
        public bool IsPasswordExpired
        { get; set; }

        public int CanWorkAsCM {get; set;}

       public Boolean CanWrokAsCM {get;set;}
        public string Token { get; set; }
        public int result { get; set; }

    }

    //   public class LoginModel
    // {
       
    //     public string username
    //     { get; set; }

    //     public string password
    //     { get; set; }
        

    // }
}