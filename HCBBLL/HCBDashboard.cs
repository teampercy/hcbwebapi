using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBNewAPI.HCBBLL
{
    public class HCBDashboard
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;


        public HCBDashboard(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"];
        }

        public DataTable GetDashboardTopFiveAssessorList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardTop5BrokerList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", usertype);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    //  if (ds != null & ds.Tables[0].Rows.Count < 1)
                    // ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetDashboardTopFiveSchemeList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardTop5SchemeList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", usertype);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    //  if (ds != null & ds.Tables[0].Rows.Count < 1)
                    // ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetddlBrokerList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardBrokerList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", usertype);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    //  if (ds != null & ds.Tables[0].Rows.Count < 1)
                    // ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetddlSchemeList(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardSchemeList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", usertype);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    //  if (ds != null & ds.Tables[0].Rows.Count < 1)
                    // ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetScheme(int UserId, string UserType, bool IsOpen, int SchemeId, string SchemeStartDate, string SchemeEndDate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDate", conn)
                    // GetDashboardSchemeCasesList
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@Usertype ", UserType);
                   // cmd.Parameters.AddWithValue("@Isopen", IsOpen);
                    cmd.Parameters.AddWithValue("@clientId ", SchemeId);
                               
                    cmd.Parameters.AddWithValue("@StartDate", (string.IsNullOrEmpty(SchemeStartDate) || SchemeStartDate=="null") ? null : SchemeStartDate);
                    cmd.Parameters.AddWithValue("@EndDate",   (string.IsNullOrEmpty(SchemeEndDate) || SchemeStartDate=="null") ? null : SchemeEndDate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetDashboardAverageRTWDuration(int UserId, string UserType, int clientId, int typeId, string RTWStartDate, string RTWEndDate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardAverageRTWDuration", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@clientId", clientId);
                    cmd.Parameters.AddWithValue("@TypeId", typeId);                                           
                    cmd.Parameters.AddWithValue("@startdate",  (string.IsNullOrEmpty(RTWStartDate) || RTWStartDate=="null")? null : RTWStartDate);
                    cmd.Parameters.AddWithValue("@enddate",  (string.IsNullOrEmpty(RTWEndDate) || RTWStartDate=="null") ? null : RTWEndDate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetDashboardClaimantAverageAge(int UserId, string UserType, int AssessorAgeId, int SchemeAgeId, string AgeStartDate, string AgeEndDate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardClaimantAverageAge", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    //cmd.Parameters.AddWithValue("@AssessorId", AssessorAgeId);
                    cmd.Parameters.AddWithValue("@SchemeId", SchemeAgeId);                                         
                    cmd.Parameters.AddWithValue("@startdate", (string.IsNullOrEmpty(AgeStartDate) || AgeStartDate=="null") ? null : AgeStartDate);
                    cmd.Parameters.AddWithValue("@enddate",  (string.IsNullOrEmpty(AgeEndDate) || AgeEndDate=="null") ? null : AgeEndDate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
        }


        public DataTable GetHCBDashboardTotalValues(int UserId, string UserType)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetHCBDashboardTotalValues", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);

                    dashboardAdapter.Fill(ds);
                    for(int i=0; i<ds.Tables[0].Columns.Count; i++)
                    {
                        if(ds.Tables[0].Rows[0][i].ToString() =="")
                        {
                     ds.Tables[0].Rows[0][i] =0;
                        }
                     // string result1 = ds.Tables[0].Rows[0][7].ToString();

                }
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }


        public DataTable GetListForTotalValues(int Index, int UserId, string UserType, int ClientTypeId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetListForTotalValues", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@ClientTypeId", ClientTypeId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@Index", Index);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];

        }




        public DataTable GetDashboardReturnToWorkReason(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetTotalClaimPerReturnToWorkReason", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", usertype);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);

                    //  if (ds != null & ds.Tables[0].Rows.Count < 1)
                    // ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }


        public DataTable GetDashboardReasonForReferal(int UserId, string usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetTotalClaimPerInjury", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", usertype);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    //  if (ds != null & ds.Tables[0].Rows.Count < 1)
                    // ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }


        public DataTable GetDashboardInterventionDuration(int UserId, string UserType, int InterventionSectorId, int InterventionClientId, string InterventionStartDate, string InterventionEndDate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardAverageClaimDuration", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@TypeId", InterventionSectorId);
                    cmd.Parameters.AddWithValue("@ClientId", InterventionClientId);                                          
                    cmd.Parameters.AddWithValue("@startdate", (string.IsNullOrEmpty(InterventionStartDate) ||  InterventionStartDate=="null")? null : InterventionStartDate);
                    cmd.Parameters.AddWithValue("@enddate", (string.IsNullOrEmpty(InterventionEndDate) ||  InterventionEndDate =="null")? null : InterventionEndDate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
        }



        public DataTable GetDashboardClaimantGender(int UserId, string UserType,int TypeId, int ClientId, string ClaimantGenderStartDate, string ClaimantGenderEndDate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardClaimantGenderPercentage", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@TypeId", TypeId);
                    cmd.Parameters.AddWithValue("@SchemeId", ClientId);                                     
                    cmd.Parameters.AddWithValue("@startdate",(string.IsNullOrEmpty(ClaimantGenderStartDate) ||  ClaimantGenderStartDate=="null") ? null : ClaimantGenderStartDate);
                    cmd.Parameters.AddWithValue("@enddate",   (string.IsNullOrEmpty(ClaimantGenderEndDate) ||  ClaimantGenderEndDate=="null")? null : ClaimantGenderEndDate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetDashboardClosedCaseAnalysis(int UserId, string UserType,int typeId, int ClientId, string ClosedCaseStartDate, string ClosedCaseEndDate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardClaimClosedReasonPercentage", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@TypeId", typeId);
                    cmd.Parameters.AddWithValue("@ClientId ", ClientId);
                    cmd.Parameters.AddWithValue("@startdate", (ClosedCaseStartDate == " " || ClosedCaseStartDate == null || ClosedCaseStartDate=="null")? null : ClosedCaseStartDate);
                    cmd.Parameters.AddWithValue("@enddate", (ClosedCaseEndDate == " " || ClosedCaseEndDate == null || ClosedCaseEndDate=="null") ? null : ClosedCaseEndDate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
        }


        public DataTable GetDashboardReasonReferalGrid(int UserId, string UserType,int ClienttypeId, int ClientId, string ReasonReferalStartDate, string ReasonReferalEndDate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardIllnessInjuryPercentage", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@TypeId", ClienttypeId);
                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    cmd.Parameters.AddWithValue("@startdate", (ReasonReferalStartDate == " " || ReasonReferalStartDate == null  || ReasonReferalStartDate == "null")? null : ReasonReferalStartDate);
                    cmd.Parameters.AddWithValue("@enddate", (ReasonReferalEndDate == " " || ReasonReferalEndDate == null ||  ReasonReferalEndDate == "null") ? null : ReasonReferalEndDate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
        }

        
        public DataTable GetClientBySectorGrid(int ClientTypeId,int UserId,string UserType)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("HCBClientDdlForSector", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientTypeId", ClientTypeId);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
        }

       public DataTable GetCaseActivityDefinedByWeek(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDateVerticalBar", conn)//ClientActivityByDefinedWeek
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@Duration", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

 public DataTable GetBPSDetails(int UserId,string UserType,string RFRTypeId,string RFRClientId,string BPSStartDate,string BPSEndDate)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetBPSDetails", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@TypeId", RFRTypeId);                             
                    cmd.Parameters.AddWithValue("@ClientID", RFRClientId);                  
                    cmd.Parameters.AddWithValue("@startdate", (BPSStartDate == " " || BPSStartDate == null || BPSStartDate=="null")? null : BPSStartDate);
                    cmd.Parameters.AddWithValue("@enddate", (BPSEndDate == " " || BPSEndDate == null || BPSEndDate=="null") ? null : BPSEndDate);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

public DataTable GetSECBPSDetails(int UserId,string UserType,string RFRTypeId,string RFRClientId,string BPSStartDate,string BPSEndDate)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetSECBPSDetails", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@usertype", UserType);
                    cmd.Parameters.AddWithValue("@UserId", UserId);
                    cmd.Parameters.AddWithValue("@TypeId", RFRTypeId);                             
                    cmd.Parameters.AddWithValue("@ClientID", RFRClientId);                  
                    cmd.Parameters.AddWithValue("@startdate", (BPSStartDate == " " || BPSStartDate == null || BPSStartDate=="null")? null : BPSStartDate);
                    cmd.Parameters.AddWithValue("@enddate", (BPSEndDate == " " || BPSEndDate == null || BPSEndDate=="null") ? null : BPSEndDate);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }


        public DataTable BPSByDefinedDates(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("BPSByDefinedWeek", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@WeekInfo", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }
         public DataTable SECBPSByDefinedDates(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SECBPSByDefinedWeek", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@WeekInfo", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }
  public DataTable CaseCloseReasonOutcome(int UserId,string UserType,string StartDate,string EndDate)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetTotalCloseCaseReason", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                     cmd.Parameters.AddWithValue("@UserId", UserId);
                     cmd.Parameters.AddWithValue("@startdate", (StartDate == " " || StartDate == null || StartDate=="null")? null : StartDate);
                    cmd.Parameters.AddWithValue("@enddate", (EndDate == " " || EndDate == null || EndDate=="null") ? null : EndDate);

                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

      public DataTable ActivityByDefinedDateMaster(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientAactivityByDefinedDateMaster", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@WeekInfo", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }
        public DataTable GetDashboardAreaChartOPN(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDateDetailsOPN", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@Duration", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

 public DataTable GetDashboardAreaChartCLS(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDateDetailsCLS", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@Duration", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

 public DataTable GetDashboardAreaChartOPNR(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDateDetailsOPNR", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@Duration", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

 public DataTable GetDashboardAreaChartOPNI(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDateDetailsOPNI", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@Duration", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

 public DataTable GetDashboardAreaChartCLSI(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDateDetailsCLSI", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                              cmd.Parameters.AddWithValue("@Duration", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }

 public DataTable GetDashboardAreaChartCLSR(int Week,int UserId,string UserType)
       {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("ClientActivityByDefinedDateDetailsCLSR", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Usertype", UserType);
                         cmd.Parameters.AddWithValue("@UserId", UserId);
                         cmd.Parameters.AddWithValue("@Duration", Week);
                    // cmd.Parameters.AddWithValue("@UserId", UserId);
                    // cmd.Parameters.AddWithValue("@UserType", UserType);
                  
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];
       }



    }
}
