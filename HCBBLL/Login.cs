using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBNewAPI.HCBBLL
{
    public class Login
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;
        public Login(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"];
        }

        public UserModel GetLoginUser(UserModel user)
        {
            
            UserModel userModel = new UserModel();
            DataSet ds = new DataSet();
            {
                userModel.result = 0;
                try
                {
                    using (SqlConnection conn = new SqlConnection(connectionstring))
                    {
                        conn.Open();

                        if (!string.IsNullOrEmpty(user.username) && !string.IsNullOrEmpty(user.password))
                        {

                            SqlCommand cmd = new SqlCommand("usp_UsersGetDetail", conn)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                            if (!string.IsNullOrEmpty(user.username))
                            {
                                // this.db.AddInParameter(cmd, "EmailAdd", DbType.String, user.username);
                                cmd.Parameters.AddWithValue("@EmailAdd", user.username);
                            }
                            else
                            {
                                // this.db.AddInParameter(cmd, "EmailAdd", DbType.String, DBNull.Value);
                                cmd.Parameters.AddWithValue("@EmailAdd", null);
                            }
                            if (!string.IsNullOrEmpty(user.password))
                            {
                                // this.db.AddInParameter(cmd, "password", DbType.String, user.password);
                                cmd.Parameters.AddWithValue("@password", user.password);
                            }
                            else
                            {
                                // this.db.AddInParameter(cmd, "password", DbType.String, DBNull.Value);
                                cmd.Parameters.AddWithValue("@password", null);
                            }
                            SqlDataAdapter loginAdapter = new SqlDataAdapter(cmd);
                            loginAdapter.Fill(ds);
                            // ds = conn.ExecuteDataSet(cmd);
                            if (ds != null && ds.Tables.Count > 0)
                            {
                                ds.Tables[0].TableName = "UserInfo";


                                if (ds.Tables["UserInfo"].Rows.Count > 0)
                                {
                                    string pwd = Encryption.Decrypt(ds.Tables["UserInfo"].Rows[0]["Password"].ToString());
                                    DataRow dr = ds.Tables["UserInfo"].Rows[0];
                                    userModel.FaildAttempt = Convert.ToInt16(dr["FailedAttempt"]);
                                    if (userModel.FaildAttempt < 5)
                                    {
                                        if (Convert.ToBoolean(dr["IsValid"]))
                                        {
                                            try
                                            {
                                                userModel.AccountActive = Convert.ToInt32(dr["AccountActive"]);
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.AccountActive = 1;
                                            }

                                            try
                                            {
                                                userModel.Address = dr["Address"].ToString();
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.Address = "";
                                            }

                                            try
                                            {
                                                userModel.username = dr["EmailAddress"].ToString();
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.username = "";
                                            }

                                            try
                                            {
                                                userModel.ClientId = Convert.ToInt32(dr["ClientId"]);
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.ClientId = 0;
                                            }

                                            try
                                            {
                                                userModel.Name = dr["FullName"].ToString();
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.Name = "";
                                            }

                                            try
                                            {
                                                userModel.NominatedNurseId = Convert.ToInt32(dr["NominatedNurseID"]);
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.NominatedNurseId = 0;
                                            }

                                            try
                                            {
                                                userModel.password = dr["Password"].ToString();
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.password = "";
                                            }

                                            try
                                            {
                                                userModel.PhoneNum = dr["TelephoneNumber"].ToString();
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.PhoneNum = "";
                                            }

                                            try
                                            {
                                                userModel.TermsAccepted = Convert.ToInt32(dr["TermsAccepted"]);
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.TermsAccepted = 0;
                                            }

                                            try
                                            {
                                                userModel.UserId = Convert.ToInt32(dr["UserID"]);
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.UserId = 0;
                                            }

                                            try
                                            {
                                                userModel.UserType = dr["UserType"].ToString();
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.UserType = "";
                                            }

                                            try
                                            {
                                                userModel.UserTypeId = Convert.ToInt32(dr["UserTypeId"]);
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.UserTypeId = 0;
                                            }

                                            try
                                            {
    userModel.CanWorkAsCM=Convert.ToInt32(dr["CanWrokAsCM"]);
}
catch(Exception ex)
{
  userModel.CanWorkAsCM=0;   
}
                                            try
                                            {
                                                userModel.PasswordExpiryDate = Convert.ToDateTime(string.IsNullOrEmpty(dr["PasswordExpiryDate"].ToString()) ? DateTime.Now.AddDays(-1).ToString() : dr["PasswordExpiryDate"].ToString());
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.PasswordExpiryDate = DateTime.MinValue;
                                            }
                                            userModel.IsPasswordExpired = Convert.ToBoolean(dr["IsPasswordExpired"]);
                                            try
                                            {
                                                userModel.LastLogin = string.IsNullOrEmpty(dr["LastLoginDate"].ToString()) ? "" : dr["LastLoginDate"].ToString();
                                            }
                                            catch (Exception ex)
                                            {
                                                userModel.LastLogin = "";
                                            }
                                            //if (FaildAttempt > 0)
                                            //{

                                            if (userModel.AccountActive == 1)
                                            {
                                                int mnth = GetAccessMonth(userModel.UserId, userModel.LastLogin);
                                                mnth = 1; //remove this
                                                if (mnth >= 6)
                                                {
                                                    InActiveAccount(userModel.UserId);

                                                    //result = "Your account is locked Please contact administrator";
                                                    // userModel.result = "Some Error Occured.";
                                                    userModel.result = 1;


                                                }
                                                else
                                                {
                                                    //query = "Update users set FailedAttempt = 0 ,LastLoginDate = now() where EmailAddress = '" + this.EmailAdd + "'";

                                                    // query = "usp_UsersFailedAttempUpdate";
                                                    // cmd = new OleDbCommand(query, conn);
                                                    // cmd.CommandType = CommandType.StoredProcedure;
                                                    SqlCommand cmd1 = new SqlCommand("usp_UsersFailedAttempUpdate", conn)
                                                    {
                                                        CommandType = CommandType.StoredProcedure
                                                    };
                                                    if (!string.IsNullOrEmpty(user.username))
                                                    {
                                                        // this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, user.username);
                                                        cmd1.Parameters.AddWithValue("@EmailAdd", user.username);
                                                    }
                                                    else
                                                    {
                                                        // this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, DBNull.Value);
                                                        cmd1.Parameters.AddWithValue("@EmailAdd", null);
                                                    }
                                                    cmd1.ExecuteNonQuery();

                                                    //DbCommand com1 = this.db.GetSqlStringCommand(query);
                                                    //com1.CommandType = CommandType.Text;
                                                    //this.db.ExecuteNonQuery(com1);
                                                }
                                            }
                                            else if (userModel.AccountActive == 0)
                                            {
                                                //result = "Your account is locked Please contact administrator";
                                                //    userModel.result = "Your account is locked. Please contact administrator.";
                                                userModel.result = 2;

                                            }
                                        }
                                        else
                                        {

                                            //query = "Update users set FailedAttempt = FailedAttempt +1 where EmailAddress = '" + this.EmailAdd + "'";
                                            //cmd = this.db.GetSqlStringCommand(query);
                                            //cmd.CommandType = CommandType.Text;
                                            //this.db.ExecuteNonQuery(cmd);
                                            using (SqlConnection conn1 = new SqlConnection(connectionstring))
                                            {

                                                // query = "usp_UsersFailedLoginUpdate";
                                                // cmd = new OleDbCommand(query, conn1);
                                                // cmd.CommandType = CommandType.StoredProcedure;

                                                conn.Open();
                                                SqlCommand cmd2 = new SqlCommand("usp_UsersFailedLoginUpdate", conn)
                                                {
                                                    CommandType = CommandType.StoredProcedure
                                                };
                                                if (!string.IsNullOrEmpty(user.username))
                                                {
                                                    // this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, user.username);
                                                    cmd2.Parameters.AddWithValue("@EmailAdd", user.username);
                                                }

                                                else
                                                {
                                                    // this.db.AddInParameter(cmd, "@EmailAdd", DbType.String, DBNull.Value);
                                                    cmd2.Parameters.AddWithValue("@EmailAdd", null);
                                                }
                                                cmd2.ExecuteNonQuery();

                                                // userModel.result = "Invalid Login.";
                                                userModel.result = 3;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //   userModel.result = " Your account is locked. Please contact administrator";
                                        userModel.result = 2;
                                        //result = "Some Error Occured.";
                                    }
                                }
                                else
                                {
                                    //result = " Invalid user Name and Password";
                                    // userModel.result = " Please enter valid login credentials.";
                                    userModel.result = 3;
                                }
                            }
                            else
                            {
                                //result = " Invalid user Name and Password";
                                userModel.result = 3;
                            }
                            return userModel;
                        }

                        userModel.result = 1;
                        return userModel;
                    }
                }
                catch (Exception ex)
                {
                    userModel.result = 1;
                    return userModel;
                }
            }
        }

        private int GetAccessMonth(int uid, string LastLogin)
        {
            int month = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();

                    //  string LastLogDate = string.Format("{0:MM/dd/yyyy-MM-dd}", this.LastLogin);
                    DateTime dt = Convert.ToDateTime(LastLogin);
                    string LastLogDate = string.Format("{0:u}", dt);
                    int index = LastLogDate.IndexOf('Z');
                    LastLogDate = LastLogDate.Remove(index, 1);

                    // LastLogDate="2014-02-20 15:40:08";

                    // string MonthQuery = "select DateDiff(m,'" + LastLogDate + "',GETDATE()) from Users where UserID = " + this.UserId;
                    // string MonthQuery = "GetMonth";
                    // conn = DbConnection.OpenConnection();
                    // DbCommand comm = this.db.GetStoredProcCommand(MonthQuery);
                    SqlCommand cmd = new SqlCommand("GetMonth", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    if (!String.IsNullOrEmpty(LastLogDate))
                    {
                        // this.db.AddInParameter(cmd, "LastLogDate", DbType.String, LastLogDate);
                        cmd.Parameters.AddWithValue("@LastLogDate", LastLogDate);
                    }
                    else
                    {
                        // this.db.AddInParameter(cmd, "LastLogDate", DbType.String, DBNull.Value);
                        cmd.Parameters.AddWithValue("@LastLogDate", null);
                    }
                    if (uid > 0)
                    {

                        // this.db.AddInParameter(cmd, "UserId", DbType.Int32,uid);
                        cmd.Parameters.AddWithValue("@UserId", uid);
                    }
                    else
                    {
                        // this.db.AddInParameter(cmd, "UserId", DbType.Int32, DBNull.Value);
                        cmd.Parameters.AddWithValue("@UserId", null);
                    }


                    month = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {
                // DbConnection.CloseConnection(conn);
                return month;
            }

            return month;
        }

        private bool InActiveAccount(int userid)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateUsersActivation", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (userid > 0)
                    {
                        // this.db.AddInParameter(cmd, "UserId", DbType.Int32, userid);
                        cmd.Parameters.AddWithValue("@UserId", userid);
                    }
                    else
                    {
                        // this.db.AddInParameter(cmd, "UserId", DbType.Int32, DBNull.Value);
                        cmd.Parameters.AddWithValue("@UserId",null);
                    }
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

    }
}
