using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBNewAPI.HCBBLL
{
    public class UserRecord
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;


        public UserRecord(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"]; //DefualtConnectionstring
        }

        public DataSet GetUserRecordList(UserRecordModel model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetAllUsers", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@pageIndex", model.pageIndex);
                    cmd.Parameters.AddWithValue("@pageSize", model.pageSize);
                    cmd.Parameters.AddWithValue("@sort", model.sort);
                    cmd.Parameters.AddWithValue("@direction", model.direction);
                    cmd.Parameters.AddWithValue("@FullName", model.FullName);
                    cmd.Parameters.AddWithValue("@UserTypeId", model.UserTypeId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            ds.Tables[0].TableName = "CRMUserRecordGrid";
            ds.Tables[1].TableName = "HCBUsersLength";
            return (ds);
        }

        public DataTable GetUserRecordById(int UserID)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetUserFromID", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);

                    //
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        // DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        ds.Tables[0].Rows[0]["Password"] = Encryption.Decrypt(Convert.ToString(dt.Rows[0]["Password"]));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataSet CheckCRMUserEmail(string EmailAddress, int UserId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CheckEmailAddress", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@EmailAddress", EmailAddress);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            ds.Tables[0].TableName = "EmailCheck";
            return (ds);
        }
        public DataTable GetCRMUserType()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetUserTypeList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int UpdateCRMUserRecord(AddUpdatecrmUser model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateUser", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@UserID", model.UserID);
                    cmd.Parameters.AddWithValue("@EmailAddress", model.EmailAddress);
                    cmd.Parameters.AddWithValue("@Password", model.Password);
                    cmd.Parameters.AddWithValue("@FullName", model.FullName);
                    cmd.Parameters.AddWithValue("@TelephoneNumber", model.TelephoneNumber);
                    cmd.Parameters.AddWithValue("@Address", model.Address);
                    cmd.Parameters.AddWithValue("@City", model.City);
                    cmd.Parameters.AddWithValue("@UserTypeId", model.UserTypeId);
                    // cmd.Parameters.AddWithValue("@LastLoginDate", model.LastLoginDate);
                    cmd.Parameters.AddWithValue("@ActiveAccount", model.AccountActive);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.ModifiedBy);
                    cmd.Parameters.AddWithValue("@SPCompanyId", model.SPComapny);
                     if(model.CanWrokAsCM==true)
                    {
                    cmd.Parameters.AddWithValue("@CanWrokAsCM", 1);
                    }else{
                       cmd.Parameters.AddWithValue("@CanWrokAsCM", 0);  
                    }
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int AddCRNUser(AddUpdatecrmUser model)
        {
            DataSet ds = new DataSet();
            int result =0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateUser", conn)  //add stored Procedure
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@UserID";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);

                    cmd.Parameters.AddWithValue("@EmailAddress", model.EmailAddress);
                    cmd.Parameters.AddWithValue("@Password", model.Password);
                    cmd.Parameters.AddWithValue("@UserTypeId", model.UserTypeId);
                    cmd.Parameters.AddWithValue("@FullName", model.FullName);
                    cmd.Parameters.AddWithValue("@TelephoneNumber", model.TelephoneNumber);
                    cmd.Parameters.AddWithValue("@Address", model.Address);
                    cmd.Parameters.AddWithValue("@City", model.City);
                    cmd.Parameters.AddWithValue("@AccountActive", model.AccountActive);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                    cmd.Parameters.AddWithValue("@SPCompanyId", model.SPComapny);
                    if(model.CanWrokAsCM==true)
                    {
                    cmd.Parameters.AddWithValue("@CanWrokAsCM", 1);
                    }else{
                       cmd.Parameters.AddWithValue("@CanWrokAsCM", 0);  
                    }
                    ds=CheckCRMUserEmail(model.EmailAddress,0);
                    int count=Convert.ToInt32(ds.Tables[0].Rows[0]["resultCount"]);
                    if(count==0){
                     result = cmd.ExecuteNonQuery();
             
                    }else{}
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
  public DataTable GetlServiceProviderlist()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetApprovedProviderddl", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

public DataTable GetCaseRecordsForUser(int UserId,int UserTypeId)
{
     DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCaseRecordsForCM", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@UserId", UserId);  
                    cmd.Parameters.AddWithValue("@UserTypeID", UserTypeId);  

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
}

    }
}