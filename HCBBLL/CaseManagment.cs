using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Excel = Microsoft.Office.Interop.Excel;
using LumenWorks.Framework.IO.Csv;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Net.Mail;
using System.Configuration;
using System.Globalization;

namespace HCBNewAPI.HCBBLL
{
    public class CaseManagment
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;
        public CaseManagment(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"];
        }

public List<ImportResult> ImportCaseManagmentList(List<ImportCases> objImportFileList,int UserId)
{
    int result=0;
     string CaseRefNo="";
    List<ImportResult> objimporterror =new List<ImportResult>();
    ImportResult objerror =new ImportResult();
   try{
     using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
             
                    
                    foreach(ImportCases data in objImportFileList)
                    {
                               SqlCommand cmd = new SqlCommand("ImportCaseRecord", conn)
                       {
                        CommandType = CommandType.StoredProcedure
                       };
                        SqlParameter outPutParameter = new SqlParameter();
                    SqlParameter outPutParameter2 = new SqlParameter();
                    outPutParameter.ParameterName = "@HCBReferenceId";
                    outPutParameter2.ParameterName = "@StatusId";
                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;
                    outPutParameter2.SqlDbType = System.Data.SqlDbType.Int;
                    outPutParameter.Direction = System.Data.ParameterDirection.Output;
                    outPutParameter2.Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add(outPutParameter);
                     cmd.Parameters.Add(outPutParameter2);

                             CaseRefNo=data.First_name + " "+data.Last_name;
                             String details="";
                        // data.Name=data.Name.Trim(' ');
                        // var Name=data.Name.Split(' ',3);
               
//                      var Address1="";
// var Address2="";
// var City ="";
// var PostCode ="";
// if(data.Address !=null)
// {
//                       var Address = data.Address.Split(',');
//                       if(Address.Length==4)
//                       {
//                        Address1= Address[0].Trim(' ');
//                        Address2= Address[1].Trim(' ');
//                         City= Address[2].Trim(' ');
//                         PostCode= Address[3].Trim(' ');
//                       }
//                       else if(Address.Length>4){
//                            Address1= Address[0].Trim(' ')+" "+Address[1].Trim(' ');
//                        Address2= "";
//                         City= Address[Address.Length-2].Trim(' ');;
//                         PostCode= Address[Address.Length-1].Trim(' '); 
//                       }
//                       else{
//                             Address1= Address[0].Trim(' ');
//                        Address2= "";
//                         City= Address[Address.Length-2].Trim(' ');;
//                         PostCode= Address[Address.Length-1].Trim(' ');  
//                       }
//}

  if(!string.IsNullOrEmpty(data.DOB))
{
    if(data.DOB.Contains('-'))
    {
    data.DOB=data.DOB.Replace('-','/');
  }
  data.DOB=  String.IsNullOrEmpty(data.DOB) ? null:DateTime.ParseExact(data.DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
}


    if(!string.IsNullOrEmpty(data.Date_of_absence))
{
 if(data.Date_of_absence.Contains('-'))
    {
    data.Date_of_absence=data.Date_of_absence.Replace('-','/');
  }

 data.Date_of_absence=  String.IsNullOrEmpty(data.Date_of_absence) ? null:DateTime.ParseExact(data.Date_of_absence, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
}

   if(!string.IsNullOrEmpty(data.Date_referred))
{
 if(data.Date_referred.Contains('-'))
    {
    data.Date_referred = data.Date_referred.Replace('-','/');
  }

 data.Date_referred=  String.IsNullOrEmpty(data.Date_referred) ? null:DateTime.ParseExact(data.Date_referred, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
}


                       if(!string.IsNullOrEmpty(data.Reason_for_absence))
                         {
                           details +=data.Reason_for_absence;
                          }
                      if(!string.IsNullOrEmpty(data.Any_special_instruction))
                         {
                             
                           details = details + "\n" + data.Any_special_instruction;
                         }

                      if(data.New_claim !=null)
                      {
                      data.New_claim=data.New_claim.Trim(' ');
                      }
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                     cmd.Parameters.AddWithValue("@Address1", data.Address_1);
                       cmd.Parameters.AddWithValue("@Address2", data.Address_2);
                         cmd.Parameters.AddWithValue("@City", data.City_Town);
                         cmd.Parameters.AddWithValue("@PostCode", data.Postcode);
                         // cmd.Parameters.AddWithValue("@Any_special_instruction",data.Any_special_instruction);
                     cmd.Parameters.AddWithValue("@Any_special_instruction",details);
                    // cmd.Parameters.AddWithValue("@Any_special_instruction",string.IsNullOrEmpty(data.Any_special_instruction)?" ":data.Any_special_instruction);
                     cmd.Parameters.AddWithValue("@ClientName", data.CLIENT);
                     cmd.Parameters.AddWithValue("@ClaimantMobile", data.Mobile);
                     cmd.Parameters.AddWithValue("@ClaimantPhone", data.Landline);
                     cmd.Parameters.AddWithValue("@DOB",data.DOB);
                     cmd.Parameters.AddWithValue("@Date_of_absence", data.Date_of_absence);
                     cmd.Parameters.AddWithValue("@Date_referred", data.Date_referred);
                     cmd.Parameters.AddWithValue("@GIP", data.GIP);
                     cmd.Parameters.AddWithValue("@GPE", data.GPE);
                     cmd.Parameters.AddWithValue("@FirstName", data.First_name);
                     cmd.Parameters.AddWithValue("@LastName", data.Last_name);
                    cmd.Parameters.AddWithValue("@IllnessInjury", data.New_claim); 
                     cmd.Parameters.AddWithValue("@Scheme", data.Scheme);
                     cmd.Parameters.AddWithValue("@ServiceType", data.SERVICE);
                     cmd.Parameters.AddWithValue("@ServiceRequired", data.f2f_Telephone);
                     cmd.Parameters.AddWithValue("@CaseOwner", data.CASEOWNER);
                     result = Convert.ToInt32(cmd.ExecuteScalar());
                     if(result==1)
                     {
                          objerror =new ImportResult();
                         objerror.ClaimantFullName=data.First_name + " "+data.Last_name;
                         objerror.errorMessage="Case already exists";
                         objimporterror.Add(objerror);
                     }
                     else if(result==2)
                     {
                           objerror =new ImportResult();
                          objerror.ClaimantFullName=data.First_name + " "+data.Last_name;
                         objerror.errorMessage="Client does not exists";
                         objimporterror.Add(objerror);
                     }
                        else if(result==3)
                     {
                         objerror =new ImportResult();
                          objerror.ClaimantFullName=data.First_name + " "+data.Last_name;
                         objerror.errorMessage="Service type does not exists";
                         objimporterror.Add(objerror);
                     }
                         else if(result==4)
                     {
                          objerror =new ImportResult();
                          objerror.ClaimantFullName=data.First_name + " "+data.Last_name;
                         objerror.errorMessage="Service Required does not exists";
                         objimporterror.Add(objerror);
                     }
                             else if(result==5)
                     {
                          objerror =new ImportResult();
                          objerror.ClaimantFullName=data.First_name + " "+data.Last_name;
                         objerror.errorMessage="Service Type does Not exists for selected client";
                         objimporterror.Add(objerror);
                     }
                            else if(result==6)
                     {
                          objerror =new ImportResult();
                          objerror.ClaimantFullName=data.First_name + " "+data.Last_name;
                         objerror.errorMessage="Case Manager does not exists";
                         objimporterror.Add(objerror);
                     }
                     else if(result==0)
                     {
                         objerror =new ImportResult();
                         objerror.ClaimantFullName=data.First_name + " "+data.Last_name;
                         objerror.errorMessage="Case Created Successfully";
                         objimporterror.Add(objerror); 
                     }
                    }
                }
   }
   catch(Exception ex)
   {
         objerror =new ImportResult();
                          objerror.ClaimantFullName=CaseRefNo;
                         objerror.errorMessage=ex.Message;
                         objimporterror.Add(objerror);
   }
              
                return objimporterror;
    
  
}
public  List<ImportResult>  Import(int UserId,string filename, bool headers = true)
{
       int result=0;
       string FirstName="";
       List<ImportResult> objimporterror =new List<ImportResult>();
       ImportResult objerror =new ImportResult();
       using (SqlConnection conn = new SqlConnection(connectionstring))
         {
           conn.Open();
                   
        CsvReader objReader = new CsvReader(new StreamReader(filename), true);
  
  try{
     while (objReader.ReadNextRecord())
                {
                     SqlCommand cmd = new SqlCommand("ImportCaseRecord", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                      SqlParameter outPutParameter = new SqlParameter();
                    SqlParameter outPutParameter2 = new SqlParameter();
                    outPutParameter.ParameterName = "@HCBReferenceId";
                    outPutParameter2.ParameterName = "@StatusId";
                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;
                    outPutParameter2.SqlDbType = System.Data.SqlDbType.Int;
                    outPutParameter.Direction = System.Data.ParameterDirection.Output;
                    outPutParameter2.Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.Add(outPutParameter);
                     cmd.Parameters.Add(outPutParameter2);

                    string ServiceType=objReader[20];
                    string IllnessInjury =objReader[12];
                          String details="";
                           FirstName=objReader[0]+" "+objReader[1];
                          var Date_referred =objReader[11];
                          var DOB=objReader[4];
                           var Date_of_absence=objReader[17];
                          if(!string.IsNullOrEmpty(objReader[5]))
                           {
                           if(objReader[5].Contains('-'))
                              {
                              DOB  = objReader[5].Replace('-','/');
                              }

                             DOB =  String.IsNullOrEmpty(DOB) ? null:DateTime.ParseExact(DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                          }

                        if(!string.IsNullOrEmpty(objReader[18]))
                         {
                          if(objReader[18].Contains('-'))
                              {
                              Date_of_absence  = objReader[18].Replace('-','/');
                              }

                               Date_of_absence =  String.IsNullOrEmpty(Date_of_absence) ? null:DateTime.ParseExact(Date_of_absence, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                          }

                        if(!string.IsNullOrEmpty(objReader[12]))
                          {
                           if(objReader[12].Contains('-'))
                              {
                               Date_referred  = objReader[12].Replace('-','/');
                              }

                             Date_referred =  String.IsNullOrEmpty(Date_referred) ? null:DateTime.ParseExact(Date_referred, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                          }
                     if(!string.IsNullOrEmpty(objReader[15]))
                         {
                           details +=objReader[15];
                          }
                      if(!string.IsNullOrEmpty(objReader[16]))
                         {
                             
                           details = details + "\n" + objReader[16];
                         }
                       cmd.Parameters.AddWithValue("@UserId", UserId);
                       cmd.Parameters.AddWithValue("@FirstName", objReader[0]);
                       cmd.Parameters.AddWithValue("@LastName", objReader[1]);
                         cmd.Parameters.AddWithValue("@ClaimantGender", objReader[2]);
                        cmd.Parameters.AddWithValue("@GPE", objReader[3]);
                       cmd.Parameters.AddWithValue("@GIP", objReader[4]);
                         cmd.Parameters.AddWithValue("@DOB",DOB);
                         cmd.Parameters.AddWithValue("@Address1", objReader[6]);
                       cmd.Parameters.AddWithValue("@Address2", objReader[7]);
                       cmd.Parameters.AddWithValue("@City", objReader[8]);
                        cmd.Parameters.AddWithValue("@PostCode", objReader[9]);
                          cmd.Parameters.AddWithValue("@ClaimantPhone", objReader[10]);
                         cmd.Parameters.AddWithValue("@ClaimantMobile", objReader[11]);
                         cmd.Parameters.AddWithValue("@Date_referred",Date_referred);
                         cmd.Parameters.AddWithValue("@IllnessInjury", objReader[13]);
                         cmd.Parameters.AddWithValue("@ServiceRequired", objReader[14]); 
                         cmd.Parameters.AddWithValue("@Any_special_instruction",details);
                     cmd.Parameters.AddWithValue("@Scheme", objReader[17]);
                      cmd.Parameters.AddWithValue("@Date_of_absence",Date_of_absence);
                       cmd.Parameters.AddWithValue("@ClientName", objReader[20]);
                        cmd.Parameters.AddWithValue("@ServiceType", objReader[21]);
                        cmd.Parameters.AddWithValue("@CaseOwner", objReader[22]);
                    
                     result = Convert.ToInt32(cmd.ExecuteScalar());
                  if(result==1)
                     {
                          objerror =new ImportResult();
                         objerror.ClaimantFullName=objReader[0] + " "+objReader[1];
                         objerror.errorMessage="Case already exists";
                         objimporterror.Add(objerror);
                     }
                     else if(result==2)
                     {
                           objerror =new ImportResult();
                          objerror.ClaimantFullName=objReader[0] + " "+objReader[1];
                         objerror.errorMessage="Client does not exists";
                         objimporterror.Add(objerror);
                     }
                        else if(result==3)
                     {
                         objerror =new ImportResult();
                          objerror.ClaimantFullName=objReader[0]+ " "+objReader[1];
                         objerror.errorMessage="Service type does not exists";
                         objimporterror.Add(objerror);
                     }
                         else if(result==4)
                     {
                          objerror =new ImportResult();
                          objerror.ClaimantFullName=objReader[0]+ " "+objReader[1];
                         objerror.errorMessage="Service Required does not exists";
                         objimporterror.Add(objerror);
                     }
                             else if(result==5)
                     {
                          objerror =new ImportResult();
                          objerror.ClaimantFullName=objReader[0]+ " "+objReader[1];
                         objerror.errorMessage="Service Type does Not exists for selected client";
                         objimporterror.Add(objerror);
                     }
                            else if(result==6)
                     {
                          objerror =new ImportResult();
                          objerror.ClaimantFullName=objReader[0] + " "+objReader[1];
                         objerror.errorMessage="Case Manager does not exists";
                         objimporterror.Add(objerror);
                     }
                     else if(result==0)
                     {
                         objerror =new ImportResult();
                         objerror.ClaimantFullName=objReader[0] + " "+objReader[1];
                         objerror.errorMessage="Case Created Successfully";
                         objimporterror.Add(objerror); 
                     }
                }
            //  return result=1;  
  }
  catch(Exception ex)
  {
     objerror =new ImportResult();
                          objerror.ClaimantFullName=FirstName;//CaseRefNo;
                         objerror.errorMessage=ex.Message;
                         objimporterror.Add(objerror);
  }
  DeleteImportedResult();
    InsertImportedCaseResult(objimporterror);
                }
              
              return objimporterror;
}
 public void DeleteImportedResult()
        {
            try{
             using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteImportedCaseResult", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                   // cmd.Parameters.AddWithValue("@HCBTelephoneID", HCBTelephoneID);
                   cmd.ExecuteNonQuery();
                   // return result;
                }
            }
            catch(Exception ex)
            {

            }
        }
 public void InsertImportedCaseResult(List<ImportResult> objimporterror)
        {
              try
            {
                foreach(ImportResult item in objimporterror)
                {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertImportedCaseResult", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@ClaimantFullName", item.ClaimantFullName);
                    cmd.Parameters.AddWithValue("@ResultStatement",item.errorMessage);
                    Convert.ToInt32(cmd.ExecuteScalar());
                }
                }
            }
            catch(Exception ex)
            {

            }
        }
 public DataTable GetImportedResultList()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetImportedResultList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
        public DataSet GetCaseManagmentList(GetHCBClientGrid model)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Get_ClientLists", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
  
                    if (!string.IsNullOrEmpty(model.usertype))
                    {
                        cmd.Parameters.AddWithValue("@usertype", model.usertype);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@usertype", null);
                    }
                    if (model.UserId > 0)
                    {
                        cmd.Parameters.AddWithValue("@UserId", model.UserId);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@UserId", null);
                    }
                    //  this.db.AddInParameter(cmd, "Isopen", DbType.Boolean, Isopen);
                    cmd.Parameters.AddWithValue("@Isopen", model.Isopen);
                    cmd.Parameters.AddWithValue("@pageIndex", model.pageIndex);
                    cmd.Parameters.AddWithValue("@pageSize", model.pageSize);
                    cmd.Parameters.AddWithValue("@sort", model.sort);
                    cmd.Parameters.AddWithValue("@direction", model.direction);
                    cmd.Parameters.AddWithValue("@HCBReferenceNo", model.HCBReferenceNo);
                    cmd.Parameters.AddWithValue("@ClientId", model.ClientId);
                   // cmd.Parameters.AddWithValue("@ActContactName", model.ClientContact);
                   cmd.Parameters.AddWithValue("@AssessorId", model.AssessorId);
                    cmd.Parameters.AddWithValue("@ClaimantDOB", model.ClaimantDOB == "" ? null : model.ClaimantDOB);
                    cmd.Parameters.AddWithValue("@CaseMgrId", model.CaseMgrId);
                    cmd.Parameters.AddWithValue("@ClaimantFirstName", model.ClaimantFirstName);
                    cmd.Parameters.AddWithValue("@ClaimantLastName", model.ClaimantLastName);
                    cmd.Parameters.AddWithValue("@HCBReceivedDate", model.HCBReceivedDate == "" ? null : model.HCBReceivedDate);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return ds.Tables[0];
            ds.Tables[0].TableName = "HCBClientGrid";
            ds.Tables[1].TableName = "HCBClientLength";
            return (ds);
        }

        public DataTable GetCaseInfoById(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientCaseInfo", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetDdlPrimaryICD10Code()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDdlICD10Code", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }



        public DataTable GetddlCaseManagment()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetddlCaseManager", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetddlClient()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetddlClient", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetddlClientContacts(int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetddlClientContacts", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
 public DataTable GetddlServiceTypeForEdit()
 {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetddlServiceTypeForEdit", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    //cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetddlServiceType(int ClientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetddlServiceType", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetddlServiceRequired()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetddlServiceRequired", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }
 public int SaveCaseManagment(SaveHCBViewModel model)
        {
int result=0;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertCaseManagmentRecord", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@HCBReferenceId";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);
                   // cmd.Parameters.AddWithValue("@HCBReferenceNo", model.HCBReferenceNo);
                    cmd.Parameters.AddWithValue("@ClientID", model.SaveUpdateCaseManagmentModel.ClientID);
                    cmd.Parameters.AddWithValue("@CaseMgrId", model.SaveUpdateCaseManagmentModel.CaseMgrId);
                    cmd.Parameters.AddWithValue("@ClientContactId", model.SaveUpdateCaseManagmentModel.ClientContactId);
                    cmd.Parameters.AddWithValue("@HCBReceivedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.HCBReceivedDate) ? null : model.SaveUpdateCaseManagmentModel.HCBReceivedDate);
                    cmd.Parameters.AddWithValue("@DateReferredToCaseOwner", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.DateReferredToCaseOwner) ? null : model.SaveUpdateCaseManagmentModel.DateReferredToCaseOwner);
                    cmd.Parameters.AddWithValue("@ServiceType", model.SaveUpdateCaseManagmentModel.ServiceType);
                    cmd.Parameters.AddWithValue("@ClaimantFirstName", model.SaveUpdateCaseManagmentModel.ClaimantFirstName);
                    cmd.Parameters.AddWithValue("@ClaimantLastName", model.SaveUpdateCaseManagmentModel.ClaimantLastName);
                    cmd.Parameters.AddWithValue("@ClaimantDOB", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ClaimantDOB) ? null : model.SaveUpdateCaseManagmentModel.ClaimantDOB);
                    cmd.Parameters.AddWithValue("@ClaimantGender", model.SaveUpdateCaseManagmentModel.ClaimantGender);
                    cmd.Parameters.AddWithValue("@ClaimantPhone", model.SaveUpdateCaseManagmentModel.ClaimantPhone);
                    cmd.Parameters.AddWithValue("@ClaimantMobile", model.SaveUpdateCaseManagmentModel.ClaimantMobile);
                    cmd.Parameters.AddWithValue("@ClaimantOccupation", model.SaveUpdateCaseManagmentModel.ClaimantOccupation);
                    cmd.Parameters.AddWithValue("@ClaimantServiceReq", model.SaveUpdateCaseManagmentModel.ClaimantServiceReq);
                    cmd.Parameters.AddWithValue("@ClaimantAddress1", model.SaveUpdateCaseManagmentModel.ClaimantAddress1);
                    cmd.Parameters.AddWithValue("@ClaimantAddress2", model.SaveUpdateCaseManagmentModel.ClaimantAddress2);
                    cmd.Parameters.AddWithValue("@ClaimantCity", model.SaveUpdateCaseManagmentModel.ClaimantCity);
                    cmd.Parameters.AddWithValue("@ClaimantCounty", model.SaveUpdateCaseManagmentModel.ClaimantCounty);
                    cmd.Parameters.AddWithValue("@OccupationalClassification", model.SaveUpdateCaseManagmentModel.OccupationalClassification);
                    cmd.Parameters.AddWithValue("@EmployerName", model.SaveUpdateCaseManagmentModel.EmployerName);
                    cmd.Parameters.AddWithValue("@EmployerContact", model.SaveUpdateCaseManagmentModel.EmployerContact);
                    cmd.Parameters.AddWithValue("@EmployerEmail", model.SaveUpdateCaseManagmentModel.EmployerEmail);
                    cmd.Parameters.AddWithValue("@ClientClaimRef", model.SaveUpdateCaseManagmentModel.ClientClaimRef);
                    cmd.Parameters.AddWithValue("@DisabilityCategoryId", model.SaveUpdateCaseManagmentModel.DisabilityCategoryId);                                      
                    cmd.Parameters.AddWithValue("@DisabilityDescription", model.SaveUpdateCaseManagmentModel.DisabilityDescription);
                                                      
                    cmd.Parameters.AddWithValue("@IncapacityDefination", model.SaveUpdateCaseManagmentModel.IncapacityDefination);
                    cmd.Parameters.AddWithValue("@SchemeId", model.SaveUpdateCaseManagmentModel.SchemeId);
                    cmd.Parameters.AddWithValue("@WaitingPeriodId", model.SaveUpdateCaseManagmentModel.DeferredPeriod);
                    cmd.Parameters.AddWithValue("@EndOfBenefitDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.EndOfBenefitDate) ? null : model.SaveUpdateCaseManagmentModel.EndOfBenefitDate);
                    cmd.Parameters.AddWithValue("@MonthlyBenefit", model.SaveUpdateCaseManagmentModel.MonthlyBenefit);
                    cmd.Parameters.AddWithValue("@IsEligibleforEAP", model.SaveUpdateCaseManagmentModel.IsEligibleforEAP);
                    cmd.Parameters.AddWithValue("@SchemeNumer", model.SaveUpdateCaseManagmentModel.SchemeNumer);
                    cmd.Parameters.AddWithValue("@FirstAbsentDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.FirstAbsentDate) ? null : model.SaveUpdateCaseManagmentModel.FirstAbsentDate);
                    cmd.Parameters.AddWithValue("@IllnessInjuryId", model.SaveUpdateCaseManagmentModel.IllnessInjuryId);
                    cmd.Parameters.AddWithValue("@DisabilityType", model.SaveUpdateCaseManagmentModel.DisabilityType);
                    cmd.Parameters.AddWithValue("@ClaimClosedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ClaimClosedDate) ? null : model.SaveUpdateCaseManagmentModel.ClaimClosedDate);
                    cmd.Parameters.AddWithValue("@ClaimClosedReasonId", model.SaveUpdateCaseManagmentModel.ClaimClosedReasonId);
                    cmd.Parameters.AddWithValue("@ClaimDetails", model.SaveUpdateCaseManagmentModel.ClaimDetails);
                    cmd.Parameters.AddWithValue("@SentToNurseDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.SentToNurseDate) ? null : model.SaveUpdateCaseManagmentModel.SentToNurseDate);
                    cmd.Parameters.AddWithValue("@ClaimantFirstContactedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ClaimantFirstContactedDate) ? null : model.SaveUpdateCaseManagmentModel.ClaimantFirstContactedDate);
                    cmd.Parameters.AddWithValue("@FirstVerbalContactClaimantDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.FirstVerbalContactClaimantDate) ? null : model.SaveUpdateCaseManagmentModel.FirstVerbalContactClaimantDate);
                    cmd.Parameters.AddWithValue("@NextReviewDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.NextReviewDate) ? null : model.SaveUpdateCaseManagmentModel.NextReviewDate);
                    cmd.Parameters.AddWithValue("@FeeCharged", model.SaveUpdateCaseManagmentModel.FeeCharged);
                    cmd.Parameters.AddWithValue("@RehabCost", model.SaveUpdateCaseManagmentModel.RehabCost);
                    cmd.Parameters.AddWithValue("@DateCancelled", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.DateCancelled) ? null : model.SaveUpdateCaseManagmentModel.DateCancelled);
                    cmd.Parameters.AddWithValue("@ReturnToWorkDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ReturnToWorkDate) ? null : model.SaveUpdateCaseManagmentModel.ReturnToWorkDate);
                    cmd.Parameters.AddWithValue("@ReturnToWorkReason", model.SaveUpdateCaseManagmentModel.ReturnToWorkReason);
                    cmd.Parameters.AddWithValue("@CaseClosedReasonId", model.SaveUpdateCaseManagmentModel.CaseClosedReasonId);
                     cmd.Parameters.AddWithValue("@PDAID", model.SaveUpdateCaseManagmentModel.PDAID);
                    if (model.SaveUpdateCaseManagmentModel.ServiceType == 9)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ReportClient) ? null : model.SaveUpdateCaseManagmentModel.ReportClient);
                 //07/07/2020 PM
                    if (model.SaveUpdateCaseManagmentModel.ServiceType == 5)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ServiceCaseClosed) ? null : model.SaveUpdateCaseManagmentModel.ServiceCaseClosed);
                    if (model.SaveUpdateCaseManagmentModel.ServiceType == 8)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.DateProjectClosed) ? null : model.SaveUpdateCaseManagmentModel.DateProjectClosed);
                    if (model.SaveUpdateCaseManagmentModel.ServiceType == 7)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ContractEndDate) ? null : model.SaveUpdateCaseManagmentModel.ContractEndDate);
                    if (model.SaveUpdateCaseManagmentModel.ServiceType != 9 && model.SaveUpdateCaseManagmentModel.ServiceType != 7 && model.SaveUpdateCaseManagmentModel.ServiceType != 8 && model.SaveUpdateCaseManagmentModel.ServiceType != 5)
                    {
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.CaseClosedDate) ? null : model.SaveUpdateCaseManagmentModel.CaseClosedDate);
                    }
                    
                    cmd.Parameters.AddWithValue("@ReportSubmitted", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ReportSubmitted) ? null : model.SaveUpdateCaseManagmentModel.ReportSubmitted);
                    cmd.Parameters.AddWithValue("@TypeOfContactId", model.SaveUpdateCaseManagmentModel.TypeOfContactId);
                    cmd.Parameters.AddWithValue("@TypeOfContactDetails", model.SaveUpdateCaseManagmentModel.TypeOfContactDetails);
                  
                    cmd.Parameters.AddWithValue("@ClientRefEmail", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ClientRefEmail) ? null : model.SaveUpdateCaseManagmentModel.ClientRefEmail);
                    cmd.Parameters.AddWithValue("@ClientRefTelephone", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ClientRefTelephone) ? null : model.SaveUpdateCaseManagmentModel.ClientRefTelephone);
                  
                    cmd.Parameters.AddWithValue("@DateFirstContactApplicant", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.DateFirstContactApplicant) ? null : model.SaveUpdateCaseManagmentModel.DateFirstContactApplicant);
                    cmd.Parameters.AddWithValue("@DateClaimFormSent", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.DateClaimFormSent) ? null : model.SaveUpdateCaseManagmentModel.DateClaimFormSent);
                    cmd.Parameters.AddWithValue("@ClaimantPostcode", model.SaveUpdateCaseManagmentModel.ClaimantPostcode);
                    // cmd.Parameters.AddWithValue("@SubsequentActivity",model.SubsequentActivity);
                    cmd.Parameters.AddWithValue("@Outcome", model.SaveUpdateCaseManagmentModel.Outcome);
                    cmd.Parameters.AddWithValue("@ReasonForDeclinature", model.SaveUpdateCaseManagmentModel.ReasonForDeclinature);
                    cmd.Parameters.AddWithValue("@SubmitedTraceSmart", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.SubmitedTraceSmart) ? null : model.SaveUpdateCaseManagmentModel.SubmitedTraceSmart);
                    cmd.Parameters.AddWithValue("@DataReturned", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.DataReturned) ? null : model.SaveUpdateCaseManagmentModel.DataReturned);
                    cmd.Parameters.AddWithValue("@PositiveTraceAge", model.SaveUpdateCaseManagmentModel.PositiveTraceAge);
                    cmd.Parameters.AddWithValue("@OverallTraces", model.SaveUpdateCaseManagmentModel.OverallTraces);
                    cmd.Parameters.AddWithValue("@AnnualReportDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.AnnualReportDate) ? null : model.SaveUpdateCaseManagmentModel.AnnualReportDate);
                    cmd.Parameters.AddWithValue("@AnnualReportOutcome", model.SaveUpdateCaseManagmentModel.AnnualReportOutcome);
                    cmd.Parameters.AddWithValue("@AnnualReportValue", model.SaveUpdateCaseManagmentModel.AnnualReportValue);
                    cmd.Parameters.AddWithValue("@HCBAppointedDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.HCBAppointedDate) ? null : model.SaveUpdateCaseManagmentModel.HCBAppointedDate);
                    cmd.Parameters.AddWithValue("@NameOfTrust", model.SaveUpdateCaseManagmentModel.NameOfTrust);
                    cmd.Parameters.AddWithValue("@LiveCoveredNumber", model.SaveUpdateCaseManagmentModel.LiveCoveredNumber);
                    cmd.Parameters.AddWithValue("@ReportingIntervals", model.SaveUpdateCaseManagmentModel.ReportingIntervals);
                    cmd.Parameters.AddWithValue("@AssessorId", model.SaveUpdateCaseManagmentModel.Assessor);
                    cmd.Parameters.AddWithValue("@AssessorRate", model.SaveUpdateCaseManagmentModel.AssessorRate);
                    cmd.Parameters.AddWithValue("@ClientRate", model.SaveUpdateCaseManagmentModel.ClientRate);
                    cmd.Parameters.AddWithValue("@ContractStartDate", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ContractStartDate) ? null : model.SaveUpdateCaseManagmentModel.ContractStartDate);
                    cmd.Parameters.AddWithValue("@SubmitedToProvider", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.SubmitedToProvider) ? null : model.SaveUpdateCaseManagmentModel.SubmitedToProvider);
                    cmd.Parameters.AddWithValue("@ReportReceived", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ReportReceived) ? null : model.SaveUpdateCaseManagmentModel.ReportReceived);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.SaveUpdateCaseManagmentModel.UserId);

                    cmd.Parameters.AddWithValue("@ProviderId",model.SaveUpdateCaseManagmentModel.ServiceProviderId);
                    
                    cmd.Parameters.AddWithValue("@ReportDueBy", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.ReportDueBy) ? null : model.SaveUpdateCaseManagmentModel.ReportDueBy);
                    cmd.Parameters.AddWithValue("@PensionSchmName", model.SaveUpdateCaseManagmentModel.PensionSchmName);

                    cmd.Parameters.AddWithValue("@TimeSpent", model.SaveUpdateCaseManagmentModel.TimeSpent);
                    cmd.Parameters.AddWithValue("@FeesPayable", model.SaveUpdateCaseManagmentModel.FeesPayable);

                    cmd.Parameters.AddWithValue("@DateofAssessment", model.SaveUpdateCaseManagmentModel.DateofAssessment);
                    cmd.Parameters.AddWithValue("@Old_HCBRefId", model.SaveUpdateCaseManagmentModel.Old_HCBRefId);
                   // cmd.Parameters.AddWithValue("@ReopenStatus", Convert.ToInt32(model.SaveUpdateCaseManagmentModel.ReopenStatus));
                    cmd.Parameters.AddWithValue("@EmployerTeleNo", model.SaveUpdateCaseManagmentModel.EmployerTeleNo);
                    cmd.Parameters.AddWithValue("@EmployerContact1", model.SaveUpdateCaseManagmentModel.EmployerContact1);
                    cmd.Parameters.AddWithValue("@EmployerContact2", model.SaveUpdateCaseManagmentModel.EmployerContact2);
                    cmd.Parameters.AddWithValue("@ServiceRequestedId",model.SaveUpdateCaseManagmentModel.ServiceRequestedId);

                    cmd.Parameters.AddWithValue("@ClaimantTitle", model.SaveUpdateCaseManagmentModel.ClaimantTitle);
                    cmd.Parameters.AddWithValue("@ClaimantEmail", model.SaveUpdateCaseManagmentModel.ClaimantEmail);
                    cmd.Parameters.AddWithValue("@EmployerAddress", model.SaveUpdateCaseManagmentModel.EmployerAddress);
                    cmd.Parameters.AddWithValue("@EmployerOccupation", model.SaveUpdateCaseManagmentModel.EmployerOccupation);
                    cmd.Parameters.AddWithValue("@PDICDCode", model.SaveUpdateCaseManagmentModel.PDICDCode);
                    cmd.Parameters.AddWithValue("@TimeLag", model.SaveUpdateCaseManagmentModel.TimeLag);
                    cmd.Parameters.AddWithValue("@ClaimStatusId", model.SaveUpdateCaseManagmentModel.ClaimStatusId);
                    cmd.Parameters.AddWithValue("@ClaimStatusDate", model.SaveUpdateCaseManagmentModel.ClaimStatusDate);
                    cmd.Parameters.AddWithValue("@OutcomesId", model.SaveUpdateCaseManagmentModel.OutcomesId);
                   // cmd.Parameters.AddWithValue("@DateOfActivity", string.IsNullOrEmpty(model.SaveUpdateCaseManagmentModel.DateOfActivity) ? null : model.SaveUpdateCaseManagmentModel.DateOfActivity);
                   // cmd.Parameters.AddWithValue("@ClaimAssessorId", model.SaveUpdateCaseManagmentModel.ClaimAssessorId);
                  //  cmd.Parameters.AddWithValue("@ActionId", model.SaveUpdateCaseManagmentModel.ActionId);
                     //
                    cmd.Parameters.AddWithValue("@ReferrerName", model.SaveUpdateCaseManagmentModel.ReferrerName);
                    cmd.Parameters.AddWithValue("@ReferrerEmail", model.SaveUpdateCaseManagmentModel.ReferrerEmail);
                    cmd.Parameters.AddWithValue("@ReferrerTele", model.SaveUpdateCaseManagmentModel.ReferrerTele);


                     result = Convert.ToInt32(cmd.ExecuteScalar());
                    if(model.SaveUpdateCaseManagmentModel.CaseMgrId !=null){
                     GetCaseMgrDetails(model.SaveUpdateCaseManagmentModel.CaseMgrId,result);
                    }
if(result !=null && model.SaveHCBNote!=null)
{
           foreach (var noteItem in model.SaveHCBNote)
                        {
                            noteItem.HCBReferenceId = result;
                            noteItem.LoggedInUserId = model.SaveUpdateCaseManagmentModel.UserId;
                            SaveHCBNote(noteItem);

                        }
}
else{}

                    return result;
                }
            }
            catch (Exception ex)
            {
            return result;//throw ex;  for  Azure serevr email is not sending so commenting exception for time being

            }
        }

  public void GetCaseMgrDetails(int? CaseMgrId, int result)
        {
            DataSet ds = new DataSet();
             StringBuilder sbMailBody = new StringBuilder();
            MailMessage mail = new MailMessage();
            //string HCBRefNo = " ";
            using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCaseManagerEmail", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.AddWithValue("@result", result);
                SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                dashboardAdapter.Fill(ds);
                // string HCBRefNo="";
                // // ds.Tables[0].Rows["EmailAddress"].ToString();
                // // SendNewCaseEmail("1",result);
               
  string MailSubject = "ORCA-New Case Allocation - ReferenceNo- " + ds.Tables[0].Rows[0]["HCBReferenceNo"];

                    sbMailBody.Append("A new referral has been allocated to you, under " + ds.Tables[0].Rows[0]["HCBReferenceNo"] + ".</br>Please login to ORCA immediately and action this referral." + "<br/></br><span></span><b>ORCA Group</b>");
 

               
                mail.To.Add(ds.Tables[0].Rows[0]["EmailAddress"].ToString());
               // mail.Bcc.Add(System.Configuration.ConfigurationManager.AppSettings["BCCEmailAddress"].ToString());
                mail.Subject = MailSubject;
                mail.Body = sbMailBody.ToString();
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SmtpHost"].ToString());
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
               
               // System.Net.NetworkCredential objAuthentication = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SmtpUserName"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"].ToString());

                smtp.UseDefaultCredentials =true;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SmtpPort"]);
                smtp.EnableSsl = false;

               // smtp.Credentials = objAuthentication; 
              //  mail.ReplyTo = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
                smtp.Send(mail);
                sbMailBody.Clear();

            }
        }

    

        public int UpdateCaseManagment(SaveUpdateCaseManagmentModel model)
        {
             DataTable dt= GetCaseInfoById(model.HCBReferenceId);
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateCaseManagmentRecord", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@HCBReferenceNo", model.HCBReferenceNo);
                    cmd.Parameters.AddWithValue("@ClientID", model.ClientID);
                    cmd.Parameters.AddWithValue("@CaseMgrId", model.CaseMgrId);
                    cmd.Parameters.AddWithValue("@ClientContactId", model.ClientContactId);
                    cmd.Parameters.AddWithValue("@HCBReceivedDate", string.IsNullOrEmpty(model.HCBReceivedDate) ? null : model.HCBReceivedDate);
                    cmd.Parameters.AddWithValue("@DateReferredToCaseOwner", string.IsNullOrEmpty(model.DateReferredToCaseOwner) ? null : model.DateReferredToCaseOwner);
                    cmd.Parameters.AddWithValue("@ServiceType", model.ServiceType);
                    cmd.Parameters.AddWithValue("@ClaimantFirstName", string.IsNullOrEmpty(model.ClaimantFirstName) ? null : model.ClaimantFirstName);
                    cmd.Parameters.AddWithValue("@ClaimantLastName", string.IsNullOrEmpty(model.ClaimantLastName) ? null : model.ClaimantLastName);
                    cmd.Parameters.AddWithValue("@ClaimantDOB", string.IsNullOrEmpty(model.ClaimantDOB) ? null : model.ClaimantDOB);
                    cmd.Parameters.AddWithValue("@ClaimantGender", string.IsNullOrEmpty(model.ClaimantGender) ? null : model.ClaimantGender);
                    cmd.Parameters.AddWithValue("@ClaimantPhone", string.IsNullOrEmpty(model.ClaimantPhone) ? null : model.ClaimantPhone);
                    cmd.Parameters.AddWithValue("@ClaimantMobile", string.IsNullOrEmpty(model.ClaimantMobile) ? null : model.ClaimantMobile);
                    cmd.Parameters.AddWithValue("@ClaimantOccupation", string.IsNullOrEmpty(model.ClaimantOccupation) ? null : model.ClaimantOccupation);
                    cmd.Parameters.AddWithValue("@ClaimantServiceReq", model.ClaimantServiceReq);
                    cmd.Parameters.AddWithValue("@ClaimantAddress1", string.IsNullOrEmpty(model.ClaimantAddress1) ? null : model.ClaimantAddress1);
                    cmd.Parameters.AddWithValue("@ClaimantAddress2", string.IsNullOrEmpty(model.ClaimantAddress2) ? null : model.ClaimantAddress2);
                    cmd.Parameters.AddWithValue("@ClaimantCity", string.IsNullOrEmpty(model.ClaimantCity) ? null : model.ClaimantCity);
                    cmd.Parameters.AddWithValue("@ClaimantCounty", string.IsNullOrEmpty(model.ClaimantCounty) ? null : model.ClaimantCounty);
                    cmd.Parameters.AddWithValue("@OccupationalClassification", model.OccupationalClassification);
                    cmd.Parameters.AddWithValue("@EmployerName", string.IsNullOrEmpty(model.EmployerName) ? null : model.EmployerName);
                    cmd.Parameters.AddWithValue("@EmployerContact", string.IsNullOrEmpty(model.EmployerContact) ? null : model.EmployerContact);
                    cmd.Parameters.AddWithValue("@EmployerEmail", string.IsNullOrEmpty(model.EmployerEmail) ? null : model.EmployerEmail);
                    cmd.Parameters.AddWithValue("@ClientClaimRef", string.IsNullOrEmpty(model.ClientClaimRef) ? null : model.ClientClaimRef);
                    cmd.Parameters.AddWithValue("@DisabilityCategoryId", model.DisabilityCategoryId);
                    cmd.Parameters.AddWithValue("@DisabilityDescription", string.IsNullOrEmpty(model.DisabilityDescription) ? null : model.DisabilityDescription);
                 
                   
                    cmd.Parameters.AddWithValue("@IncapacityDefination", model.IncapacityDefination);
                    cmd.Parameters.AddWithValue("@SchemeId", model.SchemeId);
                    cmd.Parameters.AddWithValue("@WaitingPeriodId", model.DeferredPeriod);
                    cmd.Parameters.AddWithValue("@EndOfBenefitDate", string.IsNullOrEmpty(model.EndOfBenefitDate) ? null : model.EndOfBenefitDate);
                    cmd.Parameters.AddWithValue("@MonthlyBenefit", model.MonthlyBenefit);
                    cmd.Parameters.AddWithValue("@IsEligibleforEAP", model.IsEligibleforEAP);
                    cmd.Parameters.AddWithValue("@SchemeNumer", model.SchemeNumer);
                    cmd.Parameters.AddWithValue("@FirstAbsentDate", string.IsNullOrEmpty(model.FirstAbsentDate) ? null : model.FirstAbsentDate);
                    cmd.Parameters.AddWithValue("@IllnessInjuryId", model.IllnessInjuryId);
                    cmd.Parameters.AddWithValue("@DisabilityType", string.IsNullOrEmpty(model.DisabilityType) ? null : model.DisabilityType);
                    // cmd.Parameters.AddWithValue("@HCBGroupInstructedDate",model.HCBGroupInstructedDate);		 
                    cmd.Parameters.AddWithValue("@ClaimClosedDate", string.IsNullOrEmpty(model.ClaimClosedDate) ? null : model.ClaimClosedDate);
                    cmd.Parameters.AddWithValue("@ClaimClosedReasonId", model.ClaimClosedReasonId);
                    cmd.Parameters.AddWithValue("@ClaimDetails", string.IsNullOrEmpty(model.ClaimDetails) ? null : model.ClaimDetails);
                    cmd.Parameters.AddWithValue("@SentToNurseDate", string.IsNullOrEmpty(model.SentToNurseDate) ? null : model.SentToNurseDate);
                    cmd.Parameters.AddWithValue("@ClaimantFirstContactedDate", string.IsNullOrEmpty(model.ClaimantFirstContactedDate) ? null : model.ClaimantFirstContactedDate);
                    // cmd.Parameters.AddWithValue("@FirstHRContactedDate",null);			 
                    cmd.Parameters.AddWithValue("@FirstVerbalContactClaimantDate", string.IsNullOrEmpty(model.FirstVerbalContactClaimantDate) ? null : model.FirstVerbalContactClaimantDate);
                    cmd.Parameters.AddWithValue("@NextReviewDate", string.IsNullOrEmpty(model.NextReviewDate) ? null : model.NextReviewDate);
                    cmd.Parameters.AddWithValue("@FeeCharged", string.IsNullOrEmpty(model.FeeCharged) ? null : model.FeeCharged);
                    cmd.Parameters.AddWithValue("@RehabCost", string.IsNullOrEmpty(model.RehabCost) ? null : model.RehabCost);
                    cmd.Parameters.AddWithValue("@DateCancelled", string.IsNullOrEmpty(model.DateCancelled) ? null : model.DateCancelled);
                    cmd.Parameters.AddWithValue("@ReturnToWorkDate", string.IsNullOrEmpty(model.ReturnToWorkDate) ? null : model.ReturnToWorkDate);
                    cmd.Parameters.AddWithValue("@ReturnToWorkReason", string.IsNullOrEmpty(model.ReturnToWorkReason) ? null : model.ReturnToWorkReason);
                    cmd.Parameters.AddWithValue("@PDAID", model.PDAID);
                    // cmd.Parameters.AddWithValue("@CaseClosedDate",string.IsNullOrEmpty(model.CaseClosedDate)?
                    // string.IsNullOrEmpty(model.ServiceCaseClosed)?
                    // string.IsNullOrEmpty(model.ReportSubmitted)?string.IsNullOrEmpty(model.ContractEndDate)?string.IsNullOrEmpty(model.ReportClient)?
                    // null:(model.CaseClosedDate):(model.ServiceCaseClosed):(model.ReportSubmitted):(model.ContractEndDate):(model.ReportClient));
                       if (model.ServiceType == 9)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.ReportClient) ? null : model.ReportClient);
                 //07/07/2020 PM
                    if (model.ServiceType == 5)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.ServiceCaseClosed) ? null : model.ServiceCaseClosed);
                    if (model.ServiceType == 8)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.DateProjectClosed) ? null : model.DateProjectClosed);
                    if (model.ServiceType == 7)
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.ContractEndDate) ? null : model.ContractEndDate);
                    if (model.ServiceType != 9 && model.ServiceType != 7 && model.ServiceType != 8 && model.ServiceType != 5)
                    {
                        cmd.Parameters.AddWithValue("@CaseClosedDate", string.IsNullOrEmpty(model.CaseClosedDate) ? null : model.CaseClosedDate);
                    }
                    //string.IsNullOrEmpty(model.CaseClosedDate)?(string.IsNullOrEmpty(model.ServiceCaseClosed)?(string.IsNullOrEmpty(model.DateProjectClosed)?(string.IsNullOrEmpty(model.ContractEndDate)?(string.IsNullOrEmpty(model.ReportClient)?null:model.ReportClient):model.ContractEndDate):model.DateProjectClosed):model.ServiceCaseClosed):model.CaseClosedDate);
                    cmd.Parameters.AddWithValue("@ReportSubmitted", string.IsNullOrEmpty(model.ReportSubmitted) ? null : model.ReportSubmitted);
                    cmd.Parameters.AddWithValue("@TypeOfContactId", model.TypeOfContactId);
                    cmd.Parameters.AddWithValue("@TypeOfContactDetails", string.IsNullOrEmpty(model.TypeOfContactDetails) ? null : model.TypeOfContactDetails);
                   
                    cmd.Parameters.AddWithValue("@ClientRefEmail", string.IsNullOrEmpty(model.ClientRefEmail) ? null : model.ClientRefEmail);
                    cmd.Parameters.AddWithValue("@ClientRefTelephone", string.IsNullOrEmpty(model.ClientRefTelephone) ? null : model.ClientRefTelephone);
               
                    cmd.Parameters.AddWithValue("@DateFirstContactApplicant", string.IsNullOrEmpty(model.DateFirstContactApplicant) ? null : model.DateFirstContactApplicant);
                    cmd.Parameters.AddWithValue("@DateClaimFormSent", string.IsNullOrEmpty(model.DateClaimFormSent) ? null : model.DateClaimFormSent);
                    cmd.Parameters.AddWithValue("@ClaimantPostcode", string.IsNullOrEmpty(model.ClaimantPostcode) ? null : model.ClaimantPostcode);
                    // cmd.Parameters.AddWithValue("@SubsequentActivity",model.SubsequentActivity);
                    cmd.Parameters.AddWithValue("@Outcome", string.IsNullOrEmpty(model.Outcome) ? null : model.Outcome);
                    cmd.Parameters.AddWithValue("@SubmitedTraceSmart", string.IsNullOrEmpty(model.SubmitedTraceSmart) ? null : model.SubmitedTraceSmart);
                    cmd.Parameters.AddWithValue("@DataReturned", string.IsNullOrEmpty(model.DataReturned) ? null : model.DataReturned);
                    cmd.Parameters.AddWithValue("@PositiveTraceAge", string.IsNullOrEmpty(model.PositiveTraceAge) ? null : model.PositiveTraceAge);
                    cmd.Parameters.AddWithValue("@OverallTraces", model.OverallTraces);
                    cmd.Parameters.AddWithValue("@HCBAppointedDate", string.IsNullOrEmpty(model.HCBAppointedDate) ? null : model.HCBAppointedDate);
                    cmd.Parameters.AddWithValue("@NameOfTrust", model.NameOfTrust);
                    cmd.Parameters.AddWithValue("@AnnualReportDate", string.IsNullOrEmpty(model.AnnualReportDate) ? null : model.AnnualReportDate);
                    cmd.Parameters.AddWithValue("@AnnualReportOutcome", string.IsNullOrEmpty(model.AnnualReportOutcome) ? null : model.AnnualReportOutcome);
                    cmd.Parameters.AddWithValue("@AnnualReportValue", model.AnnualReportValue);
                    cmd.Parameters.AddWithValue("@LiveCoveredNumber", model.LiveCoveredNumber);
                    cmd.Parameters.AddWithValue("@ReportingIntervals", string.IsNullOrEmpty(model.ReportingIntervals) ? null : model.ReportingIntervals);
                    cmd.Parameters.AddWithValue("@AssessorId", model.Assessor);
                    cmd.Parameters.AddWithValue("@AssessorRate", model.AssessorRate);
                    cmd.Parameters.AddWithValue("@ClientRate", model.ClientRate);
                    cmd.Parameters.AddWithValue("@ContractStartDate", string.IsNullOrEmpty(model.ContractStartDate) ? null : model.ContractStartDate);
                    cmd.Parameters.AddWithValue("@SubmitedToProvider", string.IsNullOrEmpty(model.SubmitedToProvider) ? null : model.SubmitedToProvider);
                    cmd.Parameters.AddWithValue("@ReportReceived", string.IsNullOrEmpty(model.ReportReceived) ? null : model.ReportReceived);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.UserId);

                    cmd.Parameters.AddWithValue("@ReportDueBy", string.IsNullOrEmpty(model.ReportDueBy) ? null : model.ReportDueBy);
                    cmd.Parameters.AddWithValue("@PensionSchmName", model.PensionSchmName);
                   
                    cmd.Parameters.AddWithValue("@ProviderId",model.ServiceProviderId);

                    cmd.Parameters.AddWithValue("@TimeSpent", model.TimeSpent);
                    cmd.Parameters.AddWithValue("@FeesPayable", model.FeesPayable);
                    cmd.Parameters.AddWithValue("@DateofAssessment", model.DateofAssessment);
                    cmd.Parameters.AddWithValue("@EmployerTeleNo", model.EmployerTeleNo);
                    cmd.Parameters.AddWithValue("@EmployerContact1", model.EmployerContact1);
                    cmd.Parameters.AddWithValue("@EmployerContact2", model.EmployerContact2);
  cmd.Parameters.AddWithValue("@ServiceRequestedId",model.ServiceRequestedId);

           cmd.Parameters.AddWithValue("@ClaimantTitle", model.ClaimantTitle);
                    cmd.Parameters.AddWithValue("@ClaimantEmail", model.ClaimantEmail);
                    cmd.Parameters.AddWithValue("@EmployerAddress", model.EmployerAddress);
                    cmd.Parameters.AddWithValue("@EmployerOccupation", model.EmployerOccupation);
                    cmd.Parameters.AddWithValue("@PDICDCode", model.PDICDCode);
                    cmd.Parameters.AddWithValue("@TimeLag", model.TimeLag);
                    cmd.Parameters.AddWithValue("@ClaimStatusId", model.ClaimStatusId);
                    cmd.Parameters.AddWithValue("@ClaimStatusDate", model.ClaimStatusDate);
                    cmd.Parameters.AddWithValue("@OutcomesId", model.OutcomesId);
                   // cmd.Parameters.AddWithValue("@DateOfActivity", string.IsNullOrEmpty(model.DateOfActivity) ? null : model.DateOfActivity);
                   // cmd.Parameters.AddWithValue("@ClaimAssessorId", model.ClaimAssessorId);
                  //  cmd.Parameters.AddWithValue("@ActionId", model.ActionId);

                          cmd.Parameters.AddWithValue("@ReferrerName", model.ReferrerName);
                    cmd.Parameters.AddWithValue("@ReferrerEmail", model.ReferrerEmail);
                    cmd.Parameters.AddWithValue("@ReferrerTele", model.ReferrerTele);



                    //cmd.Parameters.AddWithValue("@ReportDueBy", model.ReportDueBy);

                    int result = cmd.ExecuteNonQuery();
                    //   if(Convert.ToInt32(dt.Rows[0]["CaseMgrId"])!=model.CaseMgrId)
                    // {
                    //   GetCaseMgrDetails(model.CaseMgrId,model.HCBReferenceId);
                    // }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public DataTable GetTelephoneActivityList(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetListTelephoneAcitivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReference", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetHCBTelephoneActivity(int HCBTelephoneID)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDetailTelephoneAcitivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBTelephoneID", HCBTelephoneID);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }


        public int SaveTelephoneActivity(SaveTelephoneActivityViewModel model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateTelephoneAcitivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                     SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@HCBTelephoneID";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);
                
                    cmd.Parameters.AddWithValue("@HCBReference", model.saveUpdateTelephoneActivity.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@CallDate", string.IsNullOrEmpty(model.saveUpdateTelephoneActivity.CallDate) ? null : model.saveUpdateTelephoneActivity.CallDate);
                    cmd.Parameters.AddWithValue("@CallTime", model.saveUpdateTelephoneActivity.CallTime);
                    cmd.Parameters.AddWithValue("@TypeofCall", model.saveUpdateTelephoneActivity.TypeofCall);
                    cmd.Parameters.AddWithValue("@CallExpectRTWDateOptimum", string.IsNullOrEmpty(model.saveUpdateTelephoneActivity.CallExpectRTWDateOptimum) ? null : model.saveUpdateTelephoneActivity.CallExpectRTWDateOptimum);
                    cmd.Parameters.AddWithValue("@CallExpectRTWDateMaximum", string.IsNullOrEmpty(model.saveUpdateTelephoneActivity.CallExpectRTWDateMaximum) ? null : model.saveUpdateTelephoneActivity.CallExpectRTWDateMaximum);
                    cmd.Parameters.AddWithValue("@CallCaseMngtRecom", model.saveUpdateTelephoneActivity.CallCaseMngtRecom);
                    //cmd.Parameters.AddWithValue("@CallNxtSchedule",model.CallNxtSchedule);					 
                    cmd.Parameters.AddWithValue("@FeeCharged", model.saveUpdateTelephoneActivity.FeeCharged);

                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    if (result != null)
                    {
                        foreach (var item in model.saveFailedcall)
                        {
                            item.HCBTelephoneID = result;
                            item.HCBReferenceId = model.saveUpdateTelephoneActivity.HCBReferenceId;
                            //  item.CreatedBy = model.saveCRMClientRecord.CreatedBy;
                            SaveFailedCall(item);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateTelephoneActivity(SaveUpdateTelephoneActivity model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateTelephoneAcitivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReference", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@HCBTelephoneID", model.HCBTelephoneID);
                    cmd.Parameters.AddWithValue("@CallDate", model.CallDate);
                    cmd.Parameters.AddWithValue("@CallTime", model.CallTime);
                    cmd.Parameters.AddWithValue("@TypeofCall", model.TypeofCall);
                    cmd.Parameters.AddWithValue("@CallExpectRTWDateOptimum", string.IsNullOrEmpty(model.CallExpectRTWDateOptimum) ? null : model.CallExpectRTWDateOptimum);
                    cmd.Parameters.AddWithValue("@CallExpectRTWDateMaximum", string.IsNullOrEmpty(model.CallExpectRTWDateMaximum) ? null : model.CallExpectRTWDateMaximum);
                    cmd.Parameters.AddWithValue("@CallCaseMngtRecom", model.CallCaseMngtRecom);
                    // cmd.Parameters.AddWithValue("@CallNxtSchedule",model.CallNxtSchedule);					 
                    cmd.Parameters.AddWithValue("@FeeCharged", model.FeeCharged);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteTelephoneActivity(int HCBTelephoneID)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteTelephoneAcitivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBTelephoneID", HCBTelephoneID);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int SaveFailedCall(SaveFailedCall model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateHCBFailedCall", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@HCBReference", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@FailCallDate", model.failCallDate);
                    cmd.Parameters.AddWithValue("@FailCallTime", model.failCallTime);
                    cmd.Parameters.AddWithValue("@HCBTelephoneID", model.HCBTelephoneID);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

  public DataTable GetCaseActivity(int CaseActivityId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCaseActivityDetails", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@CaseActivityId", CaseActivityId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }


  public DataTable GetCaseActivityGrid(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCaseActivityList", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

                public int SaveCaseActivity(SaveUpdateCaseActivity model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateHCBCaseActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                     cmd.Parameters.AddWithValue("@UserId", model.UserId);
                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@DateOfActivity", model.DateOfActivity);
                    cmd.Parameters.AddWithValue("@ClaimAssessorId ", model.ClaimAssessorId);
                    cmd.Parameters.AddWithValue("@ActionId", model.ActionId); 
                    cmd.Parameters.AddWithValue("@ServiceProviderId", model.ServiceProviderId);
                    cmd.Parameters.AddWithValue("@ReportDueBy", model.ReportDueBy);
                   cmd.ExecuteScalar();
                    return 1;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


           public int UpdateCaseActivityv(SaveUpdateCaseActivity model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateHCBCaseActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                     cmd.Parameters.AddWithValue("@UserId", model.UserId);
                     cmd.Parameters.AddWithValue("@CaseActivityId", model.CaseActivityId);
                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@DateOfActivity", model.DateOfActivity);
                    cmd.Parameters.AddWithValue("@ClaimAssessorId ", model.ClaimAssessorId);
                    cmd.Parameters.AddWithValue("@ActionId", model.ActionId); 
                    cmd.Parameters.AddWithValue("@ServiceProviderId", model.ServiceProviderId);
                    cmd.Parameters.AddWithValue("@ReportDueBy", model.ReportDueBy);
                   cmd.ExecuteScalar();
                    return 1;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteCaseActivity(int HCBCaseActivityId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteCaseActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBCaseActivityId", HCBCaseActivityId);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetFailedCallList(int HCBTelephoneID)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetHCBFailCall", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBTelephoneID", HCBTelephoneID);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }


        public int DeleteFailedCall(int FailedCallID)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteHCBFailCall", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@FailedCallID", FailedCallID);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetHCBVisitActivityGrid(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetHCBVisitActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetHCBVisitActivity(int HCBVisitId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDetailHCBVisitActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBVisitId", HCBVisitId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int DeleteVisitActivity(int HCBVisitId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteVisitActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBVisitId", HCBVisitId);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int SaveHCBVisitActivity(SaveUpdateHCBVisitActivity model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateHCBVisitActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@AppointmentDate", string.IsNullOrEmpty(model.AppointmentDate) ? null : model.AppointmentDate);
                    cmd.Parameters.AddWithValue("@ReportCompDate", string.IsNullOrEmpty(model.ReportCompDate) ? null : model.ReportCompDate);
                    cmd.Parameters.AddWithValue("@FeeCharged", model.FeeChargedVisit);
                    cmd.Parameters.AddWithValue("@TYpeOfVisit", model.TYpeOfVisit);
                    // cmd.Parameters.AddWithValue("@CreatedBy", model.CreatedBy);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateHCBVisitActivity(SaveUpdateHCBVisitActivity model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateHCBVisitActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBVisitId", model.HCBVisitId);
                    //  cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@AppointmentDate", string.IsNullOrEmpty(model.AppointmentDate) ? null : model.AppointmentDate);
                    cmd.Parameters.AddWithValue("@ReportCompDate", string.IsNullOrEmpty(model.ReportCompDate) ? null : model.ReportCompDate);
                    cmd.Parameters.AddWithValue("@FeeCharged", model.FeeChargedVisit);
                    cmd.Parameters.AddWithValue("@TYpeOfVisit", model.TYpeOfVisit);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveHCBDocument(UploadHCBFile model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertClientDocument", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@DocumentName", model.DocumentName);
                    cmd.Parameters.AddWithValue("@DocumentPath", model.DocumentPath);
                    cmd.Parameters.AddWithValue("@UploadedBy", model.UploadedBy);

                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetHCBDocuments(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientDocument", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

      
        public int SaveHCBNote(SaveHCBNote model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertCaseNotes", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@NoteText", model.NoteText);
                    cmd.Parameters.AddWithValue("@LoggedInUserId", model.LoggedInUserId);
                    cmd.Parameters.AddWithValue("@EmailSentTo1",null);
                    cmd.Parameters.AddWithValue("@EmailSentTo2", null);
                    cmd.Parameters.AddWithValue("@EmailSentTo3",null);
                    cmd.Parameters.AddWithValue("@EmailError", null);
                    cmd.Parameters.AddWithValue("@IsEmailSent", null);
                    cmd.Parameters.AddWithValue("@Comment", model.comment);


                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
                public string SendEmail(bool IsImportant, int usertypeId, string NoteCreatedDate, string HCBReferenceNo, int UserId,int CanWorkAsCM)
        {
             DataSet ds = new DataSet();
             string CMEmailaddress="";
           // string SupportMailAddress ="";
            string CompanyName ="";   
              List<string> MailList = new List<string>();
              object[] arry;
            StringBuilder sbMailBody = new StringBuilder();
            MailMessage mail = new MailMessage();
            // string HCBRefNo = " ";
             try
             {
                 using (SqlConnection conn = new SqlConnection(connectionstring))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("GetCaseNoteMailDetail", conn)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.AddWithValue("@HCBReferenceNo", HCBReferenceNo);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                dashboardAdapter.Fill(ds);

              CMEmailaddress=  ds.Tables[0].Rows[0]["EmailAddress"].ToString();
              CompanyName = ds.Tables[0].Rows[0]["Companyname"].ToString();
            // SupportMailAddress=  System.Configuration.ConfigurationManager.AppSettings["HCBSupportMailAddress"].ToString();
            foreach (DataRow row in ds.Tables[1].Rows)
                {

                    MailList.Add(row["EmailAddress"].ToString());

                }
                 arry = MailList.ToArray();
            }

                string MailSubject = "HCB Reference No.is " +HCBReferenceNo;
             sbMailBody.Append("New note added to the file "+HCBReferenceNo + " relating to case for "+CompanyName +". Please log on for further details."+ "<br/></br><span></span><b>HCB Group</b>");
                  
              if (usertypeId == 1 && CanWorkAsCM !=1)
                {
                   // sbMailBody.Append(fullname + " has added a new note dated " + NoteCreatedDate + " " + DateTime.Now.ToString("h:mm:ss tt") + ".Please log on for further details." + "<br/></br><span></span><b>HCB Group</b>");
                mail.To.Add(CMEmailaddress);
                mail.To.Add(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
                }
              else if(CanWorkAsCM ==1 && usertypeId == 1)
               {
                  mail.To.Add(CMEmailaddress);
                  mail.To.Add(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());    
                 foreach (string stringlist in arry)
                    {
                        if (!string.IsNullOrEmpty(stringlist))
                        {
                            mail.To.Add(stringlist);
                        }
                    }
               }

                else
                {
               mail.To.Add(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
                 }

               

                if (IsImportant == true)
                {
                    MailSubject = MailSubject + " - IMPORTANT";
                    mail.Priority = MailPriority.High;
                }
                // objLog.WriteLog("Case Note Mail Started");

                SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SmtpHost"].ToString());
                mail.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());

               // mail.Bcc.Add(System.Configuration.ConfigurationManager.AppSettings["BCCEmailAddress"].ToString());
                mail.Subject = MailSubject;
                mail.Body = sbMailBody.ToString();
                mail.IsBodyHtml = true;

               // System.Net.NetworkCredential objAuthentication = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SmtpUserName"].ToString(), System.Configuration.ConfigurationManager.AppSettings["SmtpPassword"].ToString());

                smtp.UseDefaultCredentials = true;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SmtpPort"]);
                smtp.EnableSsl = false; //make false for Production server
              //  smtp.Credentials = objAuthentication; 
                mail.ReplyTo = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
                smtp.Send(mail);
                  // return "Case Note Mail Send To Case Owner";
				   return "Success";
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
          
        }
       public int DeleteNote(int ID)
       {
         try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteHCBNote", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@NoteId", ID);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }   
       }

        public DataTable GetHCBNote(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCaseNotes", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int SaveSubsequentActivity(SaveUpdateSubsequentActivity model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertSubsequentActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter outPutParameter = new SqlParameter();

                    outPutParameter.ParameterName = "@Id";

                    outPutParameter.SqlDbType = System.Data.SqlDbType.Int;

                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    cmd.Parameters.Add(outPutParameter);

                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@SubsequentActivityName", model.SubsequentActivityName);
                    cmd.Parameters.AddWithValue("@ReminderCheck", model.ReminderCheck);
                    cmd.Parameters.AddWithValue("@ReminderDate", string.IsNullOrEmpty(model.ReminderDate) ? null : model.ReminderDate);
                    cmd.Parameters.AddWithValue("@CreatedBy", model.UserId);

                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateSubsequentActivity(SaveUpdateSubsequentActivity model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateSubsequentActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Id", model.Id);
                    cmd.Parameters.AddWithValue("@SubsequentActivityName", model.SubsequentActivityName);
                    cmd.Parameters.AddWithValue("@ReminderCheck", model.ReminderCheck);
                    cmd.Parameters.AddWithValue("@ReminderDate", string.IsNullOrEmpty(model.ReminderDate) ? null : model.ReminderDate);
                    cmd.Parameters.AddWithValue("@LastModifiedBy", model.UserId);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetSubsequentActivityGrid(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetSubsequentActivityGrid", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetSubsequentActivity(int Id)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetSubsequentActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Id", Id);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int DeleteSubsequentActivity(int Id)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteSubsequentActivity", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Id", Id);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetAnnualReportSummaryGrid(int HCBReferenceId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetAnnualReportSummary", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBReferenceId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public DataTable GetAnnualReportSummaryById(int AnnualReportId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetAnnualReportSummaryById", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@AnnualReportId", AnnualReportId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }

        public int SaveAnnualReportSummary(SaveUpdateAnnualReportSummary model)
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CreateAnnualReportSummary", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    // cmd.Parameters.AddWithValue("@AnnualReportId", model.AnnualReportId);
                    cmd.Parameters.AddWithValue("@HCBReferenceId", model.HCBReferenceId);
                    cmd.Parameters.AddWithValue("@AnnualReportDate", string.IsNullOrEmpty(model.AnnualReportDate) ? null : model.AnnualReportDate);
                    cmd.Parameters.AddWithValue("@AnnualReportOutcome", model.AnnualReportOutcome);
                    cmd.Parameters.AddWithValue("@AnnualReportValue", model.AnnualReportValue);
                    int result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateAnnualReportSummary(SaveUpdateAnnualReportSummary model)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UpdateAnnualReportSummary", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@AnnualReportId", model.AnnualReportId);
                    cmd.Parameters.AddWithValue("@AnnualReportDate", string.IsNullOrEmpty(model.AnnualReportDate) ? null : model.AnnualReportDate);
                    cmd.Parameters.AddWithValue("@AnnualReportOutcome", model.AnnualReportOutcome);
                    cmd.Parameters.AddWithValue("@AnnualReportValue", model.AnnualReportValue);

                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int DeleteAnnualReportSummary(int AnnualReportId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteAnnualReportSummary", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@AnnualReportId", AnnualReportId);
                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string DownloadDocument(int Id)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetHCBClientDocumentsById", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@DocumentId", Id);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0].Rows[0]["DocumentPath"].ToString();
        }

        public DataSet DeleteDocument(int Id)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DeleteHCBDocumnet", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@Id", Id);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataSet CheckClaimantExistOrNot(string FisrName, string LastName, string BirthDate,int HCBRefId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("CheckClaimant", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@FirstName", FisrName);
                    cmd.Parameters.AddWithValue("@LastName", LastName);
                    // cmd.Parameters.AddWithValue("@ActionName",ActionName);
                    cmd.Parameters.AddWithValue("@HCBReferenceNo",HCBRefId);
                    cmd.Parameters.AddWithValue("@BirthDate", (string.IsNullOrEmpty(BirthDate) || BirthDate == "null") ? null : BirthDate);


                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return (ds);
        }

        public int SaveCaseCancelReason(int HCBRefId, string Reason, int userid)
        {
            try
            {
                 using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("InsertCaseNotes", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@HCBReferenceId", HCBRefId);
                    cmd.Parameters.AddWithValue("@NoteText", Reason);
                    cmd.Parameters.AddWithValue("@LoggedInUserId", userid);
                    cmd.Parameters.AddWithValue("@Comment", "Cancel Case");
                    cmd.Parameters.AddWithValue("@EmailSentTo1", null);
                    cmd.Parameters.AddWithValue("@EmailSentTo2", null);
                    cmd.Parameters.AddWithValue("@EmailSentTo3", null);
                    cmd.Parameters.AddWithValue("@IsEmailSent", 0);



                    int result = cmd.ExecuteNonQuery();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

                public DataTable SearchHCBNote(string NoteInput, int clientId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SearchHCBNote", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@NoteInput", NoteInput);
                    cmd.Parameters.AddWithValue("@clientId", clientId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds.Tables[0];
        }


    }

}
