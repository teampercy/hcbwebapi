using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace HCBNewAPI.HCBBLL
{
    public class CRMDashboard
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;


        public CRMDashboard(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"];
        }

        public DataSet GetCRMMostActiveClients(string startdate, string enddate, int userId, string usertype,int clientTypeId )
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCRMMostActiveClients", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@startdate",(string.IsNullOrEmpty(startdate) || startdate=="null" || startdate=="")? null : startdate);

                   // cmd.Parameters.AddWithValue("@startdate", startdate == " " || startdate == null ? null : startdate);
                    cmd.Parameters.AddWithValue("@enddate", (string.IsNullOrEmpty(enddate) || startdate=="null" || startdate=="") ? null : enddate);
                    cmd.Parameters.AddWithValue("@usertype", usertype);
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.Parameters.AddWithValue("@ClientTypeId", clientTypeId);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    // if (ds != null & ds.Tables[0].Rows.Count < 1)
                    //     ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds;

        }
        public DataTable GetActiveClientsType()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetClientTypePercentage", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    // if (ds != null & ds.Tables[0].Rows.Count < 1)
                    //     ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetDashboardRegionPercentage()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardRegionPercentage", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                    // if (ds != null & ds.Tables[0].Rows.Count < 1)
                    //     ds = null;
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetCasesPerClient(bool IsOpen, int ClientId, string startdate, string enddate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCasesPerClient", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Isopen", IsOpen);
                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    cmd.Parameters.AddWithValue("@startdate", startdate == " " || startdate == null ? null : startdate);
                    cmd.Parameters.AddWithValue("@enddate", enddate == " " || enddate == null ? null : enddate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetCasesPerCaseMgr(bool IsOpen, int CaseMgrId, string startdate, string enddate)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCasesPerCaseManager", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.AddWithValue("@Isopen", IsOpen);
                    cmd.Parameters.AddWithValue("@CaseManagerId", CaseMgrId);
                    cmd.Parameters.AddWithValue("@startdate", startdate == " " || startdate == null ? null : startdate);
                    cmd.Parameters.AddWithValue("@enddate", enddate == " " || enddate == null ? null : enddate);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetTotalClients()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetTotalClients", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetActiveInActiveClient(int IsActive)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetActiveInActiveClients", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@IsActive",IsActive);
                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetDashboardClientsPerServices(int ServiceTypeId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCRMDashboardClientsPerServices", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@ServiceTypeId", ServiceTypeId);
                    // cmd.Parameters.AddWithValue("@Date", Date == " " || Date == null ? null : Date);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetDashboardNoActivityReport(int ClientId, int NoActivityInterval,int UserId,string Usertype)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetDashboardNoActivityReport", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@ClientId", ClientId);
                    cmd.Parameters.AddWithValue("@NoActivityInterval", NoActivityInterval);
                    cmd.Parameters.AddWithValue("@usertype", Usertype);
                    cmd.Parameters.AddWithValue("@UserId", UserId);

                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

        public DataTable GetClientListForService(int ServiceTypeId)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionstring))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetCRMClientListForService", conn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@ServiceTypeId", ServiceTypeId);


                    SqlDataAdapter dashboardAdapter = new SqlDataAdapter(cmd);
                    dashboardAdapter.Fill(ds);
                }
            }
            catch (Exception ex)
            {
                ds.Dispose();
                throw ex;
            }
            return ds.Tables[0];

        }

    }
}
