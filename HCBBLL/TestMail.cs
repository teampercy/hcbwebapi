using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using HCBNewAPI.Models;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Net.Mail;
using System.Configuration;
using System.Globalization;



namespace HCBNewAPI.HCBBLL
{
    public class TestMail
    {
        IConfiguration _configuration;
        public string connectionstring;
        private Database db;


        public TestMail(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionstring = _configuration["ConnectionStrings:DefaultConnection"]; //DefualtConnectionstring
        }

     public string  testmail(string SMTPHost,string Username ,string Password ,string FromEmail,string SenderEmail,string Port)
        {
             StringBuilder sbMailBody = new StringBuilder();
            MailMessage mail = new MailMessage();
            
            try
            {
                
            
                // objLog.WriteLog("Case Note Mail Started");
                    sbMailBody.Append( "This is test email.Please ignore." + "<br/></br><span></span><b>HCB Group</b>");
string MailSubject="Test Mail-HCB Group";
                SmtpClient smtp = new SmtpClient(SMTPHost);
                mail.From = new MailAddress(FromEmail);

                mail.To.Add(SenderEmail);
                mail.Bcc.Add(SenderEmail);
                mail.Subject = MailSubject;
                mail.Body = sbMailBody.ToString();
                mail.IsBodyHtml = true;

                System.Net.NetworkCredential objAuthentication = new System.Net.NetworkCredential(Username,Password);

                smtp.UseDefaultCredentials = false;
                smtp.Port = Convert.ToInt32(Port);
                smtp.EnableSsl = false;
                smtp.Credentials = objAuthentication;
              //  mail.ReplyTo = new MailAddress(SenderEmail);//new MailAddress(System.Configuration.ConfigurationManager.AppSettings["SendersEmailAddress"].ToString());
                smtp.Send(mail);
                return "Success";
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }
            
    
        }
    }
}
